from pathlib import Path

from family_album_site.util.image import (
    convert_for_web,
    convert_for_thumbnail,
    image_from_raw_data,
)

test_dir = Path(__file__).parent
common_dir = test_dir / ".." / ".." / ".." / "common"

with open(common_dir / "test.tif", "rb") as f:
    image_bytes = f.read()


def test_web():
    image = image_from_raw_data(image_bytes)
    web_bytes = convert_for_web(image)
    with open(test_dir / "test_web.jpg", "wb") as f:
        f.write(web_bytes)


def test_thumb():
    image = image_from_raw_data(image_bytes)
    thumb_bytes = convert_for_thumbnail(image)
    with open(test_dir / "test_thumb.jpg", "wb") as f:
        f.write(thumb_bytes)
