from family_album_site.util.auth import (
    gen_secret,
    gen_salt,
    hash_password,
    check_password,
)


def test_gen_secret():
    secret = gen_secret()
    assert isinstance(secret, bytes)
    assert len(secret) == 64


def test_gen_salt():
    salt = gen_salt()
    assert isinstance(salt, bytes)
    assert len(salt) == 16


def test_password():
    passhash = hash_password(password="hunter2", salt=b"asdfasdfasdfasdf")
    assert isinstance(passhash, bytes)

    # wrong password
    assert not check_password(
        password="wrong", salt=b"asdfasdfasdfasdf", passhash=passhash
    )

    # wrong salt
    assert not check_password(
        password="hunter2", salt=b"qwerqwerqwerqwer", passhash=passhash
    )

    assert check_password(
        password="hunter2", salt=b"asdfasdfasdfasdf", passhash=passhash
    )
