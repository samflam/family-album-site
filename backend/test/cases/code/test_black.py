import subprocess
from pathlib import Path

ROOT_PATH = Path(__file__).parent.parent.parent.parent

CODE_PATHS = [
    str(p)
    for p in [
        ROOT_PATH / "family_album_site",
        ROOT_PATH / "test",
    ]
]


def test_black():
    import runpy
    import sys

    format_changes = False
    cmd = ["black", "--check", *CODE_PATHS]
    proc = subprocess.run(cmd)
    assert proc.returncode == 0
