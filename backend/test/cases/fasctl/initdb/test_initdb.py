from pathlib import Path
import subprocess

test_dir = Path(__file__).parent
db_path = test_dir / "testdb.sqlite"


def test_initdb(monkeypatch):
    monkeypatch.chdir(test_dir)
    db_path.unlink(missing_ok=True)
    proc = subprocess.run(["fasctl", "initdb", str(db_path)])
    assert proc.returncode == 0
    assert db_path.exists()
