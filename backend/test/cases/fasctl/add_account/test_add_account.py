from pathlib import Path
import subprocess
import sqlite3
import sys

from family_album_site.db.account import lookup_account_by_email

test_dir = Path(__file__).parent
db_path = test_dir / "testdb.sqlite"
common_path = test_dir / ".." / ".." / ".." / "common"

sys.path.append(str(common_path))

from testlib import setup_test_db


def test_add_account(monkeypatch):
    monkeypatch.chdir(test_dir)
    test_db = setup_test_db(test_dir)
    test_db.conn.close()

    proc = subprocess.run(
        [
            "fasctl",
            "add-account",
            str(db_path),
            "test_admin_role",
            "--email",
            "test_add_account@example.com",
            "--password",
            "hunter2",
        ]
    )
    assert proc.returncode == 0

    with sqlite3.connect(db_path, autocommit=False) as conn:
        account = lookup_account_by_email(conn, "test_add_account@example.com")
    assert account is not None
