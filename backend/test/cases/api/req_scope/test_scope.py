import datetime
import shutil
from pathlib import Path
import os
import sys
from typing import Annotated
from binascii import hexlify

from fastapi.testclient import TestClient
from fastapi import Response, status, Depends

from family_album_site.api.main import app
from family_album_site.api.dependencies import ScopeRequirements

test_dir = Path(__file__).parent
common_path = test_dir / ".." / ".." / ".." / "common"

sys.path.append(str(common_path))

from testlib import setup_test_db

test_db = setup_test_db(test_dir)
test_db.conn.close()


@app.get("/test")
def check_scope_req(req: Annotated[None, Depends(ScopeRequirements(["test_scope"]))]):
    return Response(status_code=status.HTTP_200_OK)


def test_no_session(monkeypatch):
    monkeypatch.chdir(Path(__file__).parent)
    client = TestClient(app)
    req = client.get("/api/test")
    assert req.status_code == 401


def test_invalid_session(monkeypatch):
    monkeypatch.chdir(Path(__file__).parent)
    client = TestClient(app, cookies={"session-secret": "deadbeef"})
    req = client.get("/api/test")
    assert req.status_code == 401


def test_malformed_session(monkeypatch):
    monkeypatch.chdir(Path(__file__).parent)
    client = TestClient(app, cookies={"session-secret": "junk"})
    req = client.get("/api/test")
    assert req.status_code == 401


def test_forbidden_session(monkeypatch):
    monkeypatch.chdir(Path(__file__).parent)
    client = TestClient(
        app,
        cookies={
            "session-secret": hexlify(
                test_db.logged_in_basic_account.session_secret
            ).decode()
        },
    )
    req = client.get("/api/test")
    assert req.status_code == 403


def test_allowed_session(monkeypatch):
    monkeypatch.chdir(Path(__file__).parent)
    client = TestClient(
        app,
        cookies={
            "session-secret": hexlify(
                test_db.logged_in_admin_account.session_secret
            ).decode()
        },
    )
    req = client.get("/api/test")
    assert req.status_code == 200
