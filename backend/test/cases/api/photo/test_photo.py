from pathlib import Path
import sys
import shutil
from binascii import hexlify

from fastapi.testclient import TestClient

from family_album_site.api.main import app

test_dir = Path(__file__).parent
common_path = test_dir / ".." / ".." / ".." / "common"

sys.path.append(str(common_path))

from testlib import setup_test_db


def test_photo(monkeypatch):
    shutil.rmtree(test_dir / "teststore", ignore_errors=True)
    test_db = setup_test_db(test_dir)
    test_db.conn.close()
    client = TestClient(
        app,
        cookies={
            "session-secret": hexlify(
                test_db.logged_in_admin_account.session_secret
            ).decode()
        },
    )
    monkeypatch.chdir(Path(__file__).parent)
    res1 = client.get(f"/api/photo/{test_db.photo_id}")
    assert res1.status_code == 200

    res2 = client.post("/api/photo", json={"front_image_id": test_db.image_id})
    assert res2.status_code == 200
    res2_body = res2.json()
    id_ = res2_body["id"]

    res3 = client.get(f"/api/photo/{id_}")
    assert res2.status_code == 200
    res3_body = res3.json()

    assert res2_body["id"] == res3_body["id"]
    assert res2_body["front_image_id"] == res3_body["front_image_id"]
    assert res2_body["created_at"] == res3_body["created_at"]
    assert res2_body["published"] == res3_body["published"]
    assert res2_body["published_at"] == res3_body["published_at"]
    assert res2_body["location"] == res3_body["location"]
    assert res2_body["time"] == res3_body["time"]
    assert res2_body["layout"] == res3_body["layout"]
    assert res2_body["description"] == res3_body["description"]

    res4 = client.post(f"/api/photo/{id_}/layout?val=foo")
    assert res4.status_code == 200

    res5 = client.post(f"/api/photo/{id_}/time?val=bar")
    assert res5.status_code == 200

    res6 = client.post(f"/api/photo/{id_}/location?val=qux")
    assert res6.status_code == 200

    res7 = client.post(f"/api/photo/{id_}/description?val=asdf")
    assert res7.status_code == 200

    res8 = client.post(f"/api/photo/{id_}/publish")
    assert res8.status_code == 200

    res9 = client.get(f"/api/photo/{id_}")
    assert res9.status_code == 200
    res9_body = res9.json()
    assert res9_body["layout"] == "foo"
    assert res9_body["time"] == "bar"
    assert res9_body["location"] == "qux"
    assert res9_body["description"] == "asdf"
    assert res9_body["published"]
    assert res9_body["published_at"] is not None

    res10 = client.post(f"/api/photo/{id_}/unpublish")
    assert res10.status_code == 200

    res11 = client.get(f"/api/photo/{id_}")
    assert res11.status_code == 200
    res11_body = res11.json()
    assert not res11_body["published"]
    assert res11_body["published_at"] is None
