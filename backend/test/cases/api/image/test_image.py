import shutil
from pathlib import Path
import os
import sys
from binascii import hexlify

from fastapi.testclient import TestClient

from family_album_site.api.main import app

test_dir = Path(__file__).parent
common_path = test_dir / ".." / ".." / ".." / "common"

sys.path.append(str(common_path))

from testlib import setup_test_db


def test_image(monkeypatch):
    shutil.rmtree(test_dir / "teststore", ignore_errors=True)
    test_db = setup_test_db(test_dir)
    test_db.conn.close()
    client = TestClient(
        app,
        cookies={
            "session-secret": hexlify(
                test_db.logged_in_admin_account.session_secret
            ).decode()
        },
    )
    monkeypatch.chdir(Path(__file__).parent)
    with open(common_path / "test.tif", "rb") as f:
        res1 = client.post(
            "/api/image", files={"image_file": ("test.tif", f, "image/tiff")}
        )
    assert res1.status_code == 200
    res1 = res1.json()
    assert res1["created"]

    with open(common_path / "test.tif", "rb") as f:
        res2 = client.post(
            "/api/image", files={"image_file": ("test.tif", f, "image/tiff")}
        )
    assert res2.status_code == 200
    res2 = res2.json()
    assert not res2["created"]

    assert res1["id"] == res2["id"]
    assert res1["raw_info"] == res2["raw_info"]
    assert res1["web_info"] == res2["web_info"]
    assert res1["thumbnail_info"] == res2["thumbnail_info"]
    assert res1["uploaded_at"] == res2["uploaded_at"]

    image_id = res1["id"]

    res3 = client.get(f"/api/image/{image_id}")
    res3 = res3.json()
    assert res1["id"] == res3["id"]
    assert res1["raw_info"] == res3["raw_info"]
    assert res1["web_info"] == res3["web_info"]
    assert res1["thumbnail_info"] == res3["thumbnail_info"]
    assert res1["uploaded_at"] == res3["uploaded_at"]
