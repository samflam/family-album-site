import shutil
from pathlib import Path
import os
import sys
from hashlib import sha256
from binascii import hexlify
import base64

from fastapi.testclient import TestClient

from family_album_site.api.main import app
from family_album_site.db.extern import ExternInit, create_extern
from family_album_site.util.extern import ExternStoreFs

test_dir = Path(__file__).parent
common_path = test_dir / ".." / ".." / ".." / "common"
store_path = test_dir / "teststore"

sys.path.append(str(common_path))

from testlib import setup_test_db


def test_extern(monkeypatch):
    shutil.rmtree(store_path, ignore_errors=True)
    test_db = setup_test_db(test_dir)
    client = TestClient(app)
    monkeypatch.chdir(Path(__file__).parent)

    with open(common_path / "test.tif", "rb") as f:
        data = f.read()

    hasher = sha256()
    hasher.update(data)
    hash_sha256 = hasher.digest()
    extern_id = hexlify(hash_sha256).decode()

    store = ExternStoreFs(store_path)

    with test_db.conn:
        extern_row_id = create_extern(
            test_db.conn,
            ExternInit(
                data=data,
                hash_sha256=hash_sha256,
                mimetype="image/tiff",
                extension=".tiff",
            ),
            store,
        )

    res = client.get(f"/api/file/{extern_id}")
    res = res.json()

    assert res["id"] == extern_id
    res_data = base64.b64decode(res["data"].encode())
    assert res_data == data
    assert res["mimetype"] == "image/tiff"
    assert res["extension"] == ".tiff"
