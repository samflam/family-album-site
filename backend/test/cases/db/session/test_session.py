import datetime
import sqlite3
from pathlib import Path
import sys


from family_album_site.db.setup import init_database
from family_album_site.db.session import (
    SessionInit,
    create_session,
    lookup_session_from_secret,
)


common_path = Path(__file__).parent / ".." / ".." / ".." / "common"
sys.path.append(str(common_path))

from testlib import setup_test_db


def test_image_crud():
    test_db = setup_test_db(Path(__file__).parent)
    y3k = datetime.datetime(3000, 1, 1, 0, 0, 0)
    create_session(
        test_db.conn,
        SessionInit(
            account_id=test_db.account_id,
            secret=b"asdf",
            expires_at=y3k,
        ),
    )
    session1 = lookup_session_from_secret(test_db.conn, b"asdf")
    assert session1
    assert session1.account_id == test_db.account_id
    assert session1.expires_at == y3k

    session2 = lookup_session_from_secret(test_db.conn, b"junk")
    assert not session2
