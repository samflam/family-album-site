from pathlib import Path
import sqlite3
from family_album_site.db.setup import init_database

import os

db_path = Path(__file__).parent / "testdb.sqlite"


def test_create_db():
    db_path.unlink(missing_ok=True)
    conn = sqlite3.connect(db_path, autocommit=False)
    init_database(conn)
