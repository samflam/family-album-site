import sqlite3
from pathlib import Path

from family_album_site.db.setup import init_database
from family_album_site.db.role import create_role, read_role, RoleInit


db_path = Path(__file__).parent / "testdb.sqlite"


def test_create_role():
    db_path.unlink(missing_ok=True)
    conn = sqlite3.connect(db_path, autocommit=False)
    init_database(conn)
    rowid = create_role(conn, RoleInit(name="test_role"))
    role = read_role(conn, rowid)
    assert role.rowid == rowid
    assert role.name == "test_role"
