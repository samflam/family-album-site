import sqlite3
from pathlib import Path

from family_album_site.db.setup import init_database
from family_album_site.db.scope import create_scope, read_scope, ScopeInit


db_path = Path(__file__).parent / "testdb.sqlite"


def test_create_scope():
    db_path.unlink(missing_ok=True)
    conn = sqlite3.connect(db_path, autocommit=False)
    init_database(conn)
    rowid = create_scope(conn, ScopeInit(name="test_scope"))
    scope = read_scope(conn, rowid)
    assert scope.rowid == rowid
    assert scope.name == "test_scope"
