from pathlib import Path
import sys

from family_album_site.db.photo import PhotoInit, create_photo, read_photo

common_path = Path(__file__).parent / ".." / ".." / ".." / "common"
sys.path.append(str(common_path))

from testlib import setup_test_db, ExternStoreStub


def test_photo_crud():
    testdb = setup_test_db(Path(__file__).parent)
    store = ExternStoreStub()
    photo_id = create_photo(
        testdb.conn,
        PhotoInit(
            front_image_id=testdb.image_id,
            creator_id=testdb.account_id,
        ),
    )
    photo = read_photo(testdb.conn, photo_id)
