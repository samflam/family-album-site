import datetime
import sqlite3
from pathlib import Path
import sys
from hashlib import sha256

import pytest

from family_album_site.db.setup import init_database
from family_album_site.db.extern import ExternInit, create_extern
from family_album_site.db.image import (
    ImageInit,
    ImageAndExternInit,
    create_image,
    create_image_and_extern,
    read_image,
    read_image_and_extern_hash,
    delete_image,
)

from family_album_site.util.image import process_image

common_path = Path(__file__).parent / ".." / ".." / ".." / "common"
sys.path.append(str(common_path))

from testlib import setup_test_db, ExternStoreStub


def test_image_crud():
    conn = setup_test_db(Path(__file__).parent).conn
    c = conn.cursor()
    c.execute("select rowid from account where email = ?", ("test@example.com",))
    account_id = c.fetchone()[0]
    with open(common_path / "test.tif", "rb") as f:
        raw_data = f.read()

    image_data = process_image(raw_data)

    def sha256_hash(data: bytes) -> bytes:
        hasher = sha256()
        hasher.update(data)
        return hasher.digest()

    stub_store = ExternStoreStub()

    raw_id = create_extern(
        conn,
        ExternInit(
            data=image_data.raw_data, hash_sha256=sha256_hash(image_data.raw_data)
        ),
        store=stub_store,
    )

    web_id = create_extern(
        conn,
        ExternInit(
            data=image_data.web_data, hash_sha256=sha256_hash(image_data.web_data)
        ),
        store=stub_store,
    )

    thumbnail_id = create_extern(
        conn,
        ExternInit(
            data=image_data.thumbnail_data,
            hash_sha256=sha256_hash(image_data.thumbnail_data),
        ),
        store=stub_store,
    )

    image_id = create_image(
        conn,
        ImageInit(
            raw_id=raw_id,
            web_id=web_id,
            thumbnail_id=thumbnail_id,
            uploaded_at=datetime.datetime.now(),
            uploader_id=account_id,
        ),
    )
    image = read_image(conn, image_id)


def test_create_image_and_extern():
    testdb = setup_test_db(Path(__file__).parent)
    conn = testdb.conn
    account_id = testdb.account_id

    with open(common_path / "test.tif", "rb") as f:
        raw_data = f.read()

    image_data = process_image(raw_data)

    def sha256_hash(data: bytes) -> bytes:
        hasher = sha256()
        hasher.update(data)
        return hasher.digest()

    stub_store = ExternStoreStub()

    image_id = create_image_and_extern(
        conn,
        ImageAndExternInit(
            raw=ExternInit(
                data=image_data.raw_data, hash_sha256=sha256_hash(image_data.raw_data)
            ),
            web=ExternInit(
                data=image_data.web_data, hash_sha256=sha256_hash(image_data.web_data)
            ),
            thumbnail=ExternInit(
                data=image_data.thumbnail_data,
                hash_sha256=sha256_hash(image_data.thumbnail_data),
            ),
            uploaded_at=datetime.datetime.now(),
            uploader_id=account_id,
        ),
        store=stub_store,
    )

    image = read_image(conn, image_id)

    with pytest.raises(KeyError):
        read_image(conn, 9999)

    image_and_extern = read_image_and_extern_hash(conn, image_id)

    delete_image(conn, image_id)

    with pytest.raises(KeyError):
        image = read_image(conn, image_id)

    with pytest.raises(KeyError):
        delete_image(conn, image_id)
