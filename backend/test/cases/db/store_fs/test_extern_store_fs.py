import shutil
from pathlib import Path

from family_album_site.util.extern import ExternStoreFs

base_dir = Path(__file__).parent / "externdb"


def contents(path: Path) -> bytes:
    with open(path, "rb") as f:
        return f.read()


def test_extern_store_fs():
    shutil.rmtree(base_dir, ignore_errors=True)
    store = ExternStoreFs(base_dir=base_dir)
    store["asdf"] = b"qwer"
    store["foo"] = b"bar"

    path = base_dir / "as" / "asdf"
    assert path.is_file()
    assert contents(path) == b"qwer"

    path = base_dir / "fo" / "foo"
    assert path.is_file()
    assert contents(path) == b"bar"

    assert len(store) == 2
    assert sorted(store) == ["asdf", "foo"]

    del store["asdf"]
    assert len(store) == 1
    assert sorted(store) == ["foo"]
