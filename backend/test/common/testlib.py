import sqlite3
from pathlib import Path
from collections.abc import Iterable
from dataclasses import dataclass


from family_album_site.db.setup import init_database
from family_album_site.util.extern import AbstractExternStore
from family_album_site.util.mail import Mail, AbstractMailer

TEST_DB_NAME = "testdb.sqlite"


class ExternStoreStub(AbstractExternStore):
    def __getitem__(self, key: str, /) -> bytes:
        return b""

    def __setitem__(self, key: str, val: bytes, /):
        pass

    def __delitem__(self, key: str, /):
        pass

    def __iter__(self, /) -> Iterable[str]:
        raise NotImplementedError

    def __len__(self, /) -> int:
        raise NotImplementedError


class LogMailer(AbstractMailer):
    def __init__(self):
        self.log = []

    def send(self, mail: Mail):
        self.log.append(mail)


@dataclass
class LoginInfo:
    account_id: int
    session_secret: bytes


@dataclass
class TestDbInfo:
    conn: sqlite3.Connection
    basic_role_id: int
    admin_role_id: int
    account_id: int
    extern_id: int
    image_id: int
    photo_id: int
    logged_in_basic_account: LoginInfo
    logged_in_admin_account: LoginInfo


def setup_test_db(parent: Path) -> TestDbInfo:
    db_path = parent / TEST_DB_NAME
    db_path.unlink(missing_ok=True)
    conn = sqlite3.connect(str(db_path), autocommit=False)
    init_database(conn)
    c = conn.cursor()
    y2k = "2000-01-01T00:00:00"
    y3k = "3000-01-01T00:00:00"

    c.execute("insert into role(name) values (?)", ("test_basic_role",))
    basic_role_id = c.lastrowid

    c.execute("insert into role(name) values (?)", ("test_admin_role",))
    admin_role_id = c.lastrowid

    c.execute("insert into scope(name) values (?)", ("test_scope",))
    test_scope_id = c.lastrowid

    c.execute(
        "insert into role_scope_relation(role_id, scope_id) values (?, ?)",
        (admin_role_id, test_scope_id),
    )

    c.execute(
        "insert into account(email, passhash, salt, role_id, registered_at, last_activity_at, wants_emails) values (?, ?, ?, ?, ?, ?, ?)",
        ("test@example.com", b"", b"", basic_role_id, "", y2k, False),
    )
    account_id = c.lastrowid

    c.execute(
        "insert into account(email, passhash, salt, role_id, registered_at, last_activity_at, wants_emails) values (?, ?, ?, ?, ?, ?, ?)",
        ("logged_in_basic@example.com", b"", b"", basic_role_id, "", y2k, False),
    )
    logged_in_basic_account_id = c.lastrowid

    logged_in_basic_secret = b"basic"
    c.execute(
        "insert into session(account_id, secret, expires_at) values (?, ?, ?)",
        (logged_in_basic_account_id, logged_in_basic_secret, y3k),
    )

    c.execute(
        "insert into account(email, passhash, salt, role_id, registered_at, last_activity_at, wants_emails) values (?, ?, ?, ?, ?, ?, ?)",
        ("logged_in_admin@example.com", b"", b"", admin_role_id, "", y2k, False),
    )
    logged_in_admin_account_id = c.lastrowid

    logged_in_admin_secret = b"admin"
    c.execute(
        "insert into session(account_id, secret, expires_at) values (?, ?, ?)",
        (logged_in_admin_account_id, logged_in_admin_secret, y3k),
    )

    c.execute("insert into extern(hash_sha256) values (?)", (b"",))
    extern_id = c.lastrowid

    c.execute(
        "insert into image(raw_id, web_id, thumbnail_id, uploaded_at, uploader_id) values (?, ?, ?, ?, ?)",
        (extern_id, extern_id, extern_id, y2k, account_id),
    )
    image_id = c.lastrowid

    c.execute(
        "insert into photo(front_image_id, created_at, creator_id, published) values (?, ?, ?, ?)",
        (image_id, y2k, account_id, False),
    )
    photo_id = c.lastrowid

    conn.commit()

    return TestDbInfo(
        conn=conn,
        basic_role_id=basic_role_id,
        admin_role_id=admin_role_id,
        account_id=account_id,
        logged_in_basic_account=LoginInfo(
            account_id=logged_in_basic_account_id,
            session_secret=logged_in_basic_secret,
        ),
        logged_in_admin_account=LoginInfo(
            account_id=logged_in_admin_account_id,
            session_secret=logged_in_admin_secret,
        ),
        extern_id=extern_id,
        image_id=image_id,
        photo_id=photo_id,
    )
