create table scope(
	rowid integer primary key autoincrement,
	name text not null unique,
	description text
);

create unique index scope_name_idx on scope(name);

create table role(
	rowid integer primary key autoincrement,
	name text not null unique,
	description text
);

create unique index role_name_idx on role(name);

create table role_scope_relation(
	role_id integer not null,
	scope_id integer not null,
	foreign key(role_id) references role(rowid),
	foreign key(scope_id) references scope(rowid),
	unique(role_id, scope_id)
);

create table account(
	rowid integer primary key autoincrement,
	email text not null unique,
	passhash blob not null,
	salt blob not null,
	role_id integer not null,
	registered_at text not null,
	last_activity_at text not null,
	wants_emails integer not null,
	foreign key(role_id) references role(rowid)
);

create unique index account_email_idx on account(email);

create table session(
	account_id integer not null,
	secret blob not null,
	expires_at text not null,
	foreign key(account_id) references account(rowid)
);

create table batch(
	rowid integer primary key autoincrement,
	name text not null unique,
	owner_id integer not null,
	created_at text not null,
	foreign key(owner_id) references account(rowid)
);

create table image(
	rowid integer primary key autoincrement,
	raw_id int not null unique,
	web_id int not null,
	thumbnail_id int not null,
	uploaded_at text not null,
	uploader_id integer not null,
	foreign key(raw_id) references extern(rowid),
	foreign key(web_id) references extern(rowid),
	foreign key(thumbnail_id) references extern(rowid),
	foreign key(uploader_id) references account(rowid)
);

create table extern(
	rowid integer primary key autoincrement,
	hash_sha256 blob not null unique,
	mimetype text,
	extension text
);

create unique index extern_hash_sha256_idx on extern(hash_sha256);

create table accesslog(
	rowid integer primary key autoincrement,
	account_id not null,
	description text not null,
	read_only integer not null,
	accessed_at text not null,
	foreign key(account_id) references account(rowid)
);

create table photo(
	rowid integer primary key autoincrement,
	front_image_id int not null,
	created_at text not null,
	creator_id integer not null,
	published integer not null,
	published_at text,
	batch_id integer,
	location text,
	time text,
	layout text,
	description text,
	foreign key(front_image_id) references image(rowid),
	foreign key(creator_id) references account(rowid),
	foreign key(batch_id) references batch(rowid)
);

create table photo_extra_image_relation(
	photo_id integer not null,
	image_id integer not null,
	title text not null,
	added_at text not null,
	foreign key(photo_id) references photo(rowid),
	foreign key(image_id) references image(rowid),
	unique(photo_id, image_id)
);

create table person(
	rowid integer primary key autoincrement,
	first_name text not null,
	last_name text not null,
	full_name text not null,
	description text
);

create table photo_person_relation(
	photo_id text not null,
	person_id text not null,
	position integer not null,
	foreign key(photo_id) references photo(rowid),
	foreign key(person_id) references person(rowid),
	unique(photo_id, person_id)
);

create table invite(
	email text not null,
	secret blob not null unique,
	expires_at text not null,
	inviter_id int not null,
	foreign key(inviter_id) references account(rowid)
);

create table password_reset(
	account_id int not null,
	secret blob not null unique,
	expires_at text not null,
	foreign key(account_id) references account(rowid)
);
