import warnings
from pathlib import Path
import sqlite3


def init_database(conn: sqlite3.Connection):
    with conn:
        c = conn.cursor()
        init_script = Path(__file__).parent / "create_tables.sql"
        with open(init_script) as f:
            c.executescript(f.read())
        perm_script = Path(__file__).parent / "init_roles_scopes.sql"
        with open(perm_script) as f:
            c.executescript(f.read())


def connect(path: Path) -> sqlite3.Connection:
    exists = path.is_file()
    conn = sqlite3.connect(path, autocommit=False)
    if not exists:
        warnings.warn(f"Database not found at {path=}. Doing first time setup")
        init_database(conn)
    return conn
