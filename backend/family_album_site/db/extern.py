import sqlite3
from binascii import hexlify

import pydantic

from ..util.extern import AbstractExternStore


class ExternInit(pydantic.BaseModel):
    data: bytes
    hash_sha256: bytes
    mimetype: str | None = None
    extension: str | None = None


class Extern(ExternInit):
    rowid: int


class ExternLight(pydantic.BaseModel):
    "Excludes the actual data"
    rowid: int
    hash_sha256: bytes
    mimetype: str | None = None
    extension: str | None = None


def create_extern(
    conn: sqlite3.Connection, extern: ExternInit, store: AbstractExternStore
) -> int:
    key = hexlify(extern.hash_sha256).decode()
    store[key] = extern.data
    with conn:
        c = conn.cursor()
        c.execute(
            "insert into extern(hash_sha256, mimetype, extension) values (?, ?, ?)",
            (
                extern.hash_sha256,
                extern.mimetype,
                extern.extension,
            ),
        )
        return c.lastrowid


def lookup_or_create_extern(
    conn: sqlite3.Connection, extern: ExternInit, store: AbstractExternStore
) -> int:
    key = hexlify(extern.hash_sha256).decode()
    try:
        store[key] = extern.data
    except FileExistsError:
        pass
    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into extern(
                hash_sha256,
                mimetype,
                extension)
            values (?, ?, ?)
            on conflict do update set hash_sha256=excluded.hash_sha256
            returning rowid;""",
            (
                extern.hash_sha256,
                extern.mimetype,
                extern.extension,
            ),
        )
        val = next(c)[0]
        return val


def read_extern(
    conn: sqlite3.Connection, rowid: int, store: AbstractExternStore
) -> Extern:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from extern where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
    key = hexlify(row["hash_sha256"]).decode()
    data = store[key]
    return Extern(data=data, **row)


def lookup_extern_id_from_hash_sha256(
    conn: sqlite3.Connection, hash_sha256: bytes
) -> int:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select rowid from extern where hash_sha256 = ?;",
            (hash_sha256,),
        )
        return c.fetchone()[0]
