import sqlite3
import pydantic


class RoleInit(pydantic.BaseModel):
    name: str
    description: str | None = None


class Role(RoleInit):
    rowid: int


def create_role(conn: sqlite3.Connection, role: RoleInit) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            "insert into role(name, description) values (?, ?)",
            (role.name, role.description),
        )
        return c.lastrowid


def read_role(conn: sqlite3.Connection, rowid: int) -> Role:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from role where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
        return Role(**row)


def delete_role(conn: sqlite3.Connection, rowid: int):
    with conn:
        c = conn.cursor()
        c.execute("delete from role where rowid = ?", (rowid,))


def lookup_role_by_name(conn: sqlite3.Connection, name: str) -> Role | None:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from role where name = ?",
            (name,),
        )
        row = c.fetchone()
        if not row:
            return None
        return Role(**row)


def read_all_roles(conn: sqlite3.Connection) -> list[Role]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute("select * from role", ())
        return [Role(**row) for row in c.fetchall()]
