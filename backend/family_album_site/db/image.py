import pydantic
import datetime
import sqlite3
from binascii import hexlify

from ..util.extern import AbstractExternStore
from .extern import ExternInit, ExternLight, lookup_or_create_extern


class ImageInit(pydantic.BaseModel):
    raw_id: int
    web_id: int
    thumbnail_id: int
    uploaded_at: datetime.datetime = pydantic.Field(
        default_factory=lambda: datetime.datetime.now()
    )
    uploader_id: int


class ImageAndExternInit(pydantic.BaseModel):
    raw: ExternInit
    web: ExternInit
    thumbnail: ExternInit
    uploaded_at: datetime.datetime = pydantic.Field(
        default_factory=lambda: datetime.datetime.now()
    )
    uploader_id: int


class Image(ImageInit):
    rowid: int


class ImageAndExternHash(pydantic.BaseModel):
    rowid: int
    raw: ExternLight
    web: ExternLight
    thumbnail: ExternLight
    uploaded_at: datetime.datetime


def create_image_and_extern(
    conn: sqlite3.Connection, image: ImageAndExternInit, store: AbstractExternStore
) -> int:
    """Create the image record and any missing extern records"""
    raw_id = lookup_or_create_extern(conn, image.raw, store)
    web_id = lookup_or_create_extern(conn, image.web, store)
    thumbnail_id = lookup_or_create_extern(conn, image.thumbnail, store)

    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into image(
                raw_id,
                web_id,
                thumbnail_id,
                uploaded_at,
                uploader_id)
            values (?, ?, ?, ?, ?);""",
            (
                raw_id,
                web_id,
                thumbnail_id,
                image.uploaded_at.isoformat(),
                image.uploader_id,
            ),
        )
    return c.lastrowid


def create_image(conn: sqlite3.Connection, image: ImageInit) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into image(
                raw_id,
                web_id,
                thumbnail_id,
                uploaded_at,
                uploader_id)
            values (?, ?, ?, ?, ?);""",
            (
                image.raw_id,
                image.web_id,
                image.thumbnail_id,
                image.uploaded_at.isoformat(),
                image.uploader_id,
            ),
        )
        return c.lastrowid


def read_image(conn: sqlite3.Connection, rowid: int) -> Image:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from image where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
        if row is None:
            raise KeyError(f"No image with {rowid=}")
        return Image(**row)


def delete_image(conn: sqlite3.Connection, rowid: int):
    with conn:
        pre_changes = conn.total_changes
        c = conn.cursor()
        c.execute(
            "delete from image where rowid = ?",
            (rowid,),
        )
        if conn.total_changes == pre_changes:
            raise KeyError(f"No image with {rowid=}")


def read_image_and_extern_hash(
    conn: sqlite3.Connection, rowid: int
) -> ImageAndExternHash:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select
                image.rowid,
                ext_raw.rowid as raw_rowid,
                ext_raw.hash_sha256 as raw_hash_sha256,
                ext_raw.extension as raw_extension,
                ext_raw.mimetype as raw_mimetype,
                ext_web.rowid as web_rowid,
                ext_web.hash_sha256 as web_hash_sha256,
                ext_web.extension as web_extension,
                ext_web.mimetype as web_mimetype,
                ext_thumb.rowid as thumb_rowid,
                ext_thumb.hash_sha256 as thumb_hash_sha256,
                ext_thumb.extension as thumb_extension,
                ext_thumb.mimetype as thumb_mimetype,
                image.uploaded_at
            from
                image
            join
                extern as ext_raw on image.raw_id = ext_raw.rowid
            join
                extern as ext_web on image.web_id = ext_web.rowid
            join
                extern as ext_thumb on image.thumbnail_id = ext_thumb.rowid
            where image.rowid = ?;
            """,
            (rowid,),
        )
        row = c.fetchone()
        if row is None:
            raise KeyError(f"No image with {rowid=}")
        return ImageAndExternHash(
            rowid=row["rowid"],
            raw=ExternLight(
                rowid=row["raw_rowid"],
                hash_sha256=row["raw_hash_sha256"],
                extension=row["raw_extension"],
                mimetype=row["raw_mimetype"],
            ),
            web=ExternLight(
                rowid=row["web_rowid"],
                hash_sha256=row["web_hash_sha256"],
                extension=row["web_extension"],
                mimetype=row["web_mimetype"],
            ),
            thumbnail=ExternLight(
                rowid=row["thumb_rowid"],
                hash_sha256=row["thumb_hash_sha256"],
                extension=row["thumb_extension"],
                mimetype=row["thumb_mimetype"],
            ),
            uploaded_at=row["uploaded_at"],
        )


def lookup_image_id_from_hash_sha256(
    conn: sqlite3.Connection, hash_sha256: bytes
) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            "select image.rowid from image join extern on image.raw_id = extern.rowid where extern.hash_sha256 = ?;",
            (hash_sha256,),
        )
        row = c.fetchone()
        if row is None:
            raise LookupError(f"No image with {hash_sha256=}")
        return row[0]


def get_photo_usage(conn: sqlite3.Connection, image_id) -> list[int]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select
                photo.rowid as photo_id
            from photo
            where
                photo.front_image_id = ?
            union
            select
                photo_extra_image_relation.photo_id as photo_id
            from photo_extra_image_relation
            where
                photo_extra_image_relation.image_id = ?
            """,
            (image_id, image_id),
        )
        return [row["photo_id"] for row in c.fetchall()]


def find_images_and_extern_hash(conn: sqlite3.Connection) -> list[ImageAndExternHash]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select
                image.rowid,
                ext_raw.rowid as raw_rowid,
                ext_raw.hash_sha256 as raw_hash_sha256,
                ext_raw.extension as raw_extension,
                ext_raw.mimetype as raw_mimetype,
                ext_web.rowid as web_rowid,
                ext_web.hash_sha256 as web_hash_sha256,
                ext_web.extension as web_extension,
                ext_web.mimetype as web_mimetype,
                ext_thumb.rowid as thumb_rowid,
                ext_thumb.hash_sha256 as thumb_hash_sha256,
                ext_thumb.extension as thumb_extension,
                ext_thumb.mimetype as thumb_mimetype,
                image.uploaded_at
            from
                image
            join
                extern as ext_raw on image.raw_id = ext_raw.rowid
            join
                extern as ext_web on image.web_id = ext_web.rowid
            join
                extern as ext_thumb on image.thumbnail_id = ext_thumb.rowid
            """,
            (),
        )
        return [
            ImageAndExternHash(
                rowid=row["rowid"],
                raw=ExternLight(
                    rowid=row["raw_rowid"],
                    hash_sha256=row["raw_hash_sha256"],
                    extension=row["raw_extension"],
                    mimetype=row["raw_mimetype"],
                ),
                web=ExternLight(
                    rowid=row["web_rowid"],
                    hash_sha256=row["web_hash_sha256"],
                    extension=row["web_extension"],
                    mimetype=row["web_mimetype"],
                ),
                thumbnail=ExternLight(
                    rowid=row["thumb_rowid"],
                    hash_sha256=row["thumb_hash_sha256"],
                    extension=row["thumb_extension"],
                    mimetype=row["thumb_mimetype"],
                ),
                uploaded_at=row["uploaded_at"],
            )
            for row in c.fetchall()
        ]
