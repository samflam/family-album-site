import sqlite3
import pydantic


class ScopeInit(pydantic.BaseModel):
    name: str
    description: str | None = None


class Scope(ScopeInit):
    rowid: int


def create_scope(conn: sqlite3.Connection, scope: ScopeInit) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            "insert into scope(name, description) values (?, ?)",
            (scope.name, scope.description),
        )
        return c.lastrowid


def read_scope(conn: sqlite3.Connection, rowid: int) -> Scope:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from scope where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
        return Scope(**row)


def delete_scope(conn: sqlite3.Connection, rowid: int):
    with conn:
        c = conn.cursor()
        c.execute("delete from scope where rowid = ?", (rowid,))
