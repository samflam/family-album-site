import sqlite3
import datetime

import pydantic


class SessionInit(pydantic.BaseModel):
    account_id: int
    secret: bytes
    expires_at: datetime.datetime


class Session(SessionInit):
    pass


def create_session(conn: sqlite3.Connection, session: SessionInit):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into session(account_id, secret, expires_at)
            values (?, ?, ?)""",
            (session.account_id, session.secret, session.expires_at.isoformat()),
        )


def lookup_session_from_secret(
    conn: sqlite3.Connection, secret: bytes
) -> Session | None:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from session where secret = ?",
            (secret,),
        )
        row = c.fetchone()
        if not row:
            return None
        return Session(**row)
