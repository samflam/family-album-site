insert into role (name, description)
values
    ('viewer', 'Basic user'),
    ('editor', 'Prepares content'),
    ('admin', 'Manages content and user accounts')
on conflict(name) do nothing;

insert into scope (name, description)
values
    ('view_published_content', 'Allows browsing published photos and viewing published photo pages'),
    ('view_unpublished_content', 'Allows browsing any photos, viewing any photo pages, viewing image pages'),
    ('upload_images', 'Allows viewing the upload page and uploading images to it'),
    ('modify_photos', 'Allows creating, updating, publishing, and deleting photo pages')
on conflict(name) do nothing;


-- viewer permissions
insert into role_scope_relation (role_id, scope_id)
select role.rowid, scope.rowid
from role
join scope on scope.name in ('view_published_content')
where role.name = 'viewer'
on conflict(role_id, scope_id) do nothing;


-- editor permissions
insert into role_scope_relation (role_id, scope_id)
select role.rowid, scope.rowid
from role
join scope on scope.name in ('view_published_content', 'view_unpublished_content', 'upload_images', 'modify_photos')
where role.name = 'editor'
on conflict(role_id, scope_id) do nothing;


-- admin permissions
insert into role_scope_relation (role_id, scope_id)
select role.rowid, scope.rowid
from role
join scope on scope.name in ('view_published_content', 'view_unpublished_content', 'upload_images', 'modify_photos')
where role.name = 'admin'
on conflict(role_id, scope_id) do nothing;
