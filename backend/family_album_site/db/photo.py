import pydantic
import datetime
import sqlite3

from .image import ImageAndExternHash
from .extern import ExternLight


class PhotoInit(pydantic.BaseModel):
    front_image_id: int
    created_at: datetime.datetime = pydantic.Field(
        default_factory=datetime.datetime.now
    )
    creator_id: int
    published: bool = False
    published_at: datetime.datetime | None = None
    batch_id: int | None = None
    location: str | None = None
    time: str | None = None
    layout: str | None = None
    description: str | None = None


class Photo(PhotoInit):
    rowid: int


class PhotoAndThumbnailLight(pydantic.BaseModel):
    rowid: int
    thumbnail: ExternLight


class PhotoExtraImage(pydantic.BaseModel):
    photo_id: int
    image_id: int
    title: str
    added_at: datetime.datetime = pydantic.Field(default_factory=datetime.datetime.now)


def create_photo(conn: sqlite3.Connection, photo: PhotoInit) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into photo(
                front_image_id,
                created_at,
                creator_id,
                published,
                published_at,
                batch_id,
                location,
                time,
                layout,
                description)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""",
            (
                photo.front_image_id,
                photo.created_at.isoformat(),
                photo.creator_id,
                photo.published,
                photo.published_at.isoformat() if photo.published_at else None,
                photo.batch_id,
                photo.location,
                photo.time,
                photo.layout,
                photo.description,
            ),
        )
        return c.lastrowid


def read_photo(conn: sqlite3.Connection, rowid: int) -> Photo:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from photo where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
        return Photo(**row)


class PublishInfo(pydantic.BaseModel):
    published: bool
    published_at: datetime.datetime | None


def update_publish(conn: sqlite3.Connection, rowid: int, publish_info: PublishInfo):
    pa = publish_info.published_at
    with conn:
        c = conn.cursor()
        c.execute(
            """
            update photo
            set published = ?, published_at = ?
            where rowid = ?""",
            (
                publish_info.published,
                pa.isoformat() if isinstance(pa, datetime.datetime) else pa,
                rowid,
            ),
        )


def update_layout(conn: sqlite3.Connection, rowid: int, layout: str | None):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            update photo
            set layout = ?
            where rowid = ?""",
            (layout, rowid),
        )


def update_time(conn: sqlite3.Connection, rowid: int, time: str | None):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            update photo
            set time = ?
            where rowid = ?""",
            (time, rowid),
        )


def update_location(conn: sqlite3.Connection, rowid: int, location: str | None):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            update photo
            set location = ?
            where rowid = ?""",
            (location, rowid),
        )


def update_description(conn: sqlite3.Connection, rowid: int, description: str | None):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            update photo
            set description = ?
            where rowid = ?""",
            (description, rowid),
        )


def find_photos(conn: sqlite3.Connection) -> list[PhotoAndThumbnailLight]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select
                photo.rowid,
                ext_thumb.rowid as thumb_rowid,
                ext_thumb.hash_sha256 as thumb_hash_sha256,
                ext_thumb.extension as thumb_extension,
                ext_thumb.mimetype as thumb_mimetype
            from
                photo
            join
                image on photo.front_image_id = image.rowid
            join
                extern as ext_raw on image.raw_id = ext_raw.rowid
            join
                extern as ext_web on image.web_id = ext_web.rowid
            join
                extern as ext_thumb on image.thumbnail_id = ext_thumb.rowid
            """,
            (),
        )
        return [
            PhotoAndThumbnailLight(
                rowid=row["rowid"],
                thumbnail=ExternLight(
                    rowid=row["thumb_rowid"],
                    hash_sha256=row["thumb_hash_sha256"],
                    extension=row["thumb_extension"],
                    mimetype=row["thumb_mimetype"],
                ),
            )
            for row in c.fetchall()
        ]


def add_photo_extra_image(conn: sqlite3.Connection, photo_extra_image: PhotoExtraImage):
    with conn:
        c = conn.cursor()
        c.execute(
            """
            insert into photo_extra_image_relation(
                photo_id,
                image_id,
                title,
                added_at)
            values (?, ?, ?, ?);""",
            (
                photo_extra_image.photo_id,
                photo_extra_image.image_id,
                photo_extra_image.title,
                photo_extra_image.added_at.isoformat(),
            ),
        )


def read_photo_extra_images(
    conn: sqlite3.Connection, photo_id: int
) -> list[PhotoExtraImage]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select
                image_id,
                added_at,
                title
            from
                photo_extra_image_relation
            where
                photo_id = ?;""",
            (photo_id,),
        )
        return [
            PhotoExtraImage(
                photo_id=photo_id,
                image_id=row["image_id"],
                added_at=row["added_at"],
                title=row["title"],
            )
            for row in c.fetchall()
        ]
