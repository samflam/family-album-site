import sqlite3
import datetime

import pydantic


class AccountInit(pydantic.BaseModel):
    email: pydantic.EmailStr
    passhash: bytes
    salt: bytes
    role_id: int
    registered_at: datetime.datetime = pydantic.Field(
        default_factory=lambda: datetime.datetime.now()
    )
    last_activity_at: datetime.datetime = pydantic.Field(
        default_factory=lambda: datetime.datetime.now()
    )
    wants_emails: bool


class Account(AccountInit):
    rowid: int


def create_account(conn: sqlite3.Connection, account: AccountInit) -> int:
    with conn:
        c = conn.cursor()
        c.execute(
            (
                "insert into account(email, passhash, salt, role_id, registered_at, last_activity_at, wants_emails) "
                "values (?, ?, ?, ?, ?, ?, ?)"
            ),
            (
                account.email,
                account.passhash,
                account.salt,
                account.role_id,
                account.registered_at,
                account.last_activity_at,
                account.wants_emails,
            ),
        )
        return c.lastrowid


def read_account(conn: sqlite3.Connection, rowid: int) -> Account:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from account where rowid = ?",
            (rowid,),
        )
        row = c.fetchone()
        return Account(**row)


def lookup_account_by_email(
    conn: sqlite3.Connection, email: pydantic.EmailStr
) -> Account | None:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            "select * from account where email = ?",
            (email,),
        )
        row = c.fetchone()
        if not row:
            return None
        return Account(**row)
