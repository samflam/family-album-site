import sqlite3
import pydantic

from .scope import Scope


def enable_role_scope(conn: sqlite3.Connection, role_id: int, scope_id: int):
    with conn:
        c = conn.cursor()
        c.execute(
            "insert into role_scope_relation(role_id, scope_id) values (?, ?)",
            (role_id, scope_id),
        )


def disable_role_scope(conn: sqlite3.Connection, role_id: int, scope_id: int):
    with conn:
        c = conn.cursor()
        c.execute(
            "delete from role_scope_relation where role_id = ? and scope_id = ?",
            (role_id, scope_id),
        )


def lookup_scopes_from_account_id(
    conn: sqlite3.Connection, account_id: int
) -> list[Scope]:
    with conn:
        c = conn.cursor()
        c.row_factory = sqlite3.Row
        c.execute(
            """
            select scope.* from account
            join role_scope_relation on role_scope_relation.role_id = account.role_id
            join scope on scope.rowid = role_scope_relation.scope_id
            where account.rowid = ?
            """,
            (account_id,),
        )
        return [Scope(**row) for row in c.fetchall()]
