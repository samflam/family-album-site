from abc import ABC, abstractmethod
import pydantic


class Mail(pydantic.BaseModel):
    to_address: pydantic.EmailStr
    subject: str
    body: str


class AbstractMailer(ABC):
    @abstractmethod
    def send(self, mail: Mail):
        pass


class AwsMailer(AbstractMailer):
    def __init__(self, api_key):
        self.api_key = api_key

    def send(self, mail: Mail):
        pass


class StubMailer(AbstractMailer):
    def send(self, mail: Mail):
        pass
