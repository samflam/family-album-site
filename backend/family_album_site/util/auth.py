import os
import hashlib


def gen_secret() -> bytes:
    return os.urandom(64)


def gen_salt() -> bytes:
    return os.urandom(16)


def hash_password(password: str, salt: bytes) -> bytes:
    # Parameters based on:
    # https://cryptobook.nakov.com/mac-and-key-derivation/scrypt
    return hashlib.scrypt(password.encode(), salt=salt, n=16384, r=8, p=1)


def check_password(password: str, passhash: bytes, salt: bytes) -> bool:
    return hash_password(password, salt) == passhash
