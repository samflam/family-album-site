import re
import os
import tempfile
from pathlib import Path
from collections.abc import MutableMapping, Iterable

from pydantic import BaseModel


class AbstractExternStore(MutableMapping[str, bytes]):
    pass


class ExternStoreFs(AbstractExternStore):
    # Some filesystems have poor performance when there are too many
    # files in the same directory. Here we store files in
    # subdirectories matching their keys' first few characters to help
    # mitigate this performance degredation.
    class ParsedKey(BaseModel):
        parent: str
        filename: str

        @classmethod
        def project(
            cls, key: "str | ExternStoreFs.ParsedKey", /
        ) -> "ExternStoreFs.ParsedKey":
            if isinstance(key, cls):
                return key
            # Require lowercase incase we are on a case-insensitive filesystem
            # 64 is the length of a hex-encoded sha256 digest
            valid_key_pattern = r"[a-z0-9_-]{3,70}"
            if not re.match(valid_key_pattern, key):
                raise ValueError(f"{key=} doesn't satisfy {valid_key_pattern=}")
            return cls(
                parent=key[:2],
                filename=key,
            )

    def __init__(self, base_dir: Path):
        self.base_dir = Path(base_dir)
        os.makedirs(self.base_dir, exist_ok=True)

    def __getitem__(self, key: str, /) -> bytes:
        key = self.ParsedKey.project(key)
        path = self.base_dir / key.parent / key.filename
        try:
            with open(path, "rb") as f:
                return f.read()
        except FileNotFoundError:
            raise KeyError("f{key=} not in store")

    def __setitem__(self, key: str, val: bytes, /):
        key = self.ParsedKey.project(key)
        path = self.base_dir / key.parent / key.filename
        with tempfile.NamedTemporaryFile(
            "wb", delete=False, dir=self.base_dir
        ) as tmp_file:
            tmp_file.write(val)

        try:
            os.makedirs(self.base_dir / key.parent, exist_ok=True)
            # The intended behavior is, if the key is already in the store
            # we raise a FileExistsError. In windows this already happens
            # upon trying to rename to an existing file, but on linux
            # rename will silently overwrite the existing file. The
            # path.is_file() check is intended to recreate the windows
            # behavior in linux. There is a potential race condition if
            # someone else creates the file between our is_file and rename
            # checks. It would be good to replace this with an atomic EAFP
            # approach in the future.
            if path.is_file():
                raise FileExistsError("f{key=} already in store")
            os.rename(tmp_file.name, path)
        finally:
            try:
                os.unlink(tmp_file.name)
            except Exception:
                pass

    def __delitem__(self, key: str, /):
        key = self.ParsedKey.project(key)
        path = self.base_dir / key.parent / key.filename
        try:
            os.remove(path)
        except FileNotFoundError:
            raise KeyError(f"{key=} not in store")

    def __iter__(self) -> Iterable[str]:
        for path in self.base_dir.glob("*/*"):
            yield path.stem

    def __len__(self) -> int:
        return len(list(self.base_dir.glob("*/*")))
