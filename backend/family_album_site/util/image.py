from io import BytesIO

from pydantic import BaseModel
from PIL import Image
from wand.image import Image as WandImage


JPEG_COMPRESSION_QUALITY = 90
THUMBNAIL_MAX_DIM = (320, 180)


class ProcessedImage(BaseModel):
    raw_data: bytes
    web_data: bytes
    thumbnail_data: bytes
    mimetype: str
    extension: str


def wand_sanitize(raw_data: bytes) -> bytes:
    # Pass through imagemagick to clean up weird metadata issues
    # (for instance, some tif files containing rotations were crashing pillow)
    with WandImage(blob=raw_data) as image:
        print(image.format)
        with image.convert(image.format) as converted:
            return converted.make_blob()


def image_from_raw_data(raw_data: bytes) -> Image:
    sanitized = wand_sanitize(raw_data)
    return Image.open(BytesIO(sanitized))


def process_image(raw_data: bytes) -> ProcessedImage:
    sanitized = wand_sanitize(raw_data)
    with Image.open(BytesIO(sanitized)) as raw_image:
        return ProcessedImage(
            raw_data=raw_data,
            web_data=convert_for_web(raw_image),
            thumbnail_data=convert_for_thumbnail(raw_image),
            mimetype=get_mimetype(raw_image),
            extension=get_extension(raw_image),
        )


def convert_for_web(raw_image: Image) -> bytes:
    if raw_image.mode == "RGBA":
        raw_image = raw_image.convert("RGB")

    web_stream = BytesIO()
    raw_image.save(web_stream, format="JPEG", quality=JPEG_COMPRESSION_QUALITY)
    return web_stream.getvalue()


def convert_for_thumbnail(raw_image: Image) -> bytes:
    if raw_image.mode == "RGBA":
        raw_image = raw_image.convert("RGB")

    thumb_stream = BytesIO()
    max_width, max_height = THUMBNAIL_MAX_DIM
    aspect_ratio_target = max_width / max_height
    aspect_ratio_image = raw_image.width / raw_image.height
    if aspect_ratio_image > aspect_ratio_target:
        thumb_width = max_width
        thumb_height = int(max_width / aspect_ratio_image)
    else:
        thumb_width = int(max_height * aspect_ratio_image)
        thumb_height = max_height
    thumb_img = raw_image.resize((thumb_width, thumb_height))
    thumb_img.save(thumb_stream, format="JPEG", quality=JPEG_COMPRESSION_QUALITY)
    return thumb_stream.getvalue()


def get_extension(image: Image) -> str:
    if not image.format:
        raise ValueError("Unknown image format")
    return f".{image.format.lower()}"


def get_mimetype(image: Image) -> str:
    mimetype = image.get_format_mimetype()
    if not mimetype:
        raise ValueError("Unknown image mimetype")
    return mimetype
