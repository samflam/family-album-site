from binascii import hexlify

import pydantic


def sha256_hash_to_url(
    sha256_hash: bytes, extension: str | None, base_url: pydantic.AnyHttpUrl
) -> pydantic.AnyHttpUrl:
    hash_str = hexlify(sha256_hash).decode()
    extension = extension or ""
    return f"{base_url}/{hash_str}{extension}"
