import datetime
from hashlib import sha256
from binascii import hexlify
from typing import Annotated

import pydantic
from fastapi import APIRouter, UploadFile, Depends, Response
import sqlite3

from ...util.image import process_image
from ...db.image import (
    ImageAndExternInit,
    ExternInit,
    create_image_and_extern,
    delete_image,
    read_image_and_extern_hash,
    lookup_image_id_from_hash_sha256,
    find_images_and_extern_hash,
    get_photo_usage,
)

from ..dependencies import DatabaseDep, ExternStoreDep, AccountIdDep, ScopeRequirements
from .extern import ExternInfo

router = APIRouter(prefix="/image")


def sha256_hash(data: bytes) -> bytes:
    hasher = sha256()
    hasher.update(data)
    return hasher.digest()


class GetImageResponse(pydantic.BaseModel):
    id: int
    raw_info: ExternInfo
    web_info: ExternInfo
    thumbnail_info: ExternInfo
    uploaded_at: datetime.datetime


class SearchImageResponse(pydantic.BaseModel):
    images: list[GetImageResponse]


@router.get("/search")
def search_images(
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> SearchImageResponse:
    conn = db.connect()
    q = find_images_and_extern_hash(conn)
    return SearchImageResponse(
        images=[
            GetImageResponse(
                id=x.rowid,
                raw_info=ExternInfo(
                    id=hexlify(x.raw.hash_sha256),
                    extension=x.raw.extension,
                    mimetype=x.raw.mimetype,
                ),
                web_info=ExternInfo(
                    id=hexlify(x.web.hash_sha256),
                    extension=x.web.extension,
                    mimetype=x.web.mimetype,
                ),
                thumbnail_info=ExternInfo(
                    id=hexlify(x.thumbnail.hash_sha256),
                    extension=x.thumbnail.extension,
                    mimetype=x.thumbnail.mimetype,
                ),
                uploaded_at=x.uploaded_at,
            )
            for x in q
            # Only return images that aren't used in a photo
            if not get_photo_usage(conn, x.rowid)
        ]
    )


@router.get("/{image_id}")
def get_image(
    image_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> GetImageResponse:
    conn = db.connect()
    q = read_image_and_extern_hash(conn, image_id)
    return GetImageResponse(
        id=image_id,
        raw_info=ExternInfo(
            id=hexlify(q.raw.hash_sha256),
            extension=q.raw.extension,
            mimetype=q.raw.mimetype,
        ),
        web_info=ExternInfo(
            id=hexlify(q.web.hash_sha256),
            extension=q.web.extension,
            mimetype=q.web.mimetype,
        ),
        thumbnail_info=ExternInfo(
            id=hexlify(q.thumbnail.hash_sha256),
            extension=q.thumbnail.extension,
            mimetype=q.thumbnail.mimetype,
        ),
        uploaded_at=q.uploaded_at,
    )


class GetImageUsageResponse(pydantic.BaseModel):
    photos: list[int]


@router.get("/{image_id}/usage")
def get_image_usage(
    image_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> GetImageUsageResponse:
    conn = db.connect()
    q = get_photo_usage(conn, image_id)
    return GetImageUsageResponse(photos=q[:])


class PostImageResponse(pydantic.BaseModel):
    id: int
    raw_info: ExternInfo
    web_info: ExternInfo
    thumbnail_info: ExternInfo
    uploaded_at: datetime.datetime
    created: bool


@router.post("/")
async def post_image(
    image_file: UploadFile,
    db: DatabaseDep,
    store: ExternStoreDep,
    creator_id: AccountIdDep,
    _: Annotated[None, Depends(ScopeRequirements(["upload_images"]))],
) -> PostImageResponse:
    conn = db.connect()
    raw_data = await image_file.read()
    image_data = process_image(raw_data)
    raw_hash = sha256_hash(raw_data)

    image_and_extern_init = ImageAndExternInit(
        raw=ExternInit(
            data=image_data.raw_data,
            hash_sha256=raw_hash,
            mimetype=image_data.mimetype,
            extension=image_data.extension,
        ),
        web=ExternInit(
            data=image_data.web_data,
            hash_sha256=sha256_hash(image_data.web_data),
            mimetype="image/jpeg",
            extension=".jpg",
        ),
        thumbnail=ExternInit(
            data=image_data.thumbnail_data,
            hash_sha256=sha256_hash(image_data.thumbnail_data),
            mimetype="image/jpeg",
            extension=".jpg",
        ),
        uploaded_at=datetime.datetime.now(),
        uploader_id=creator_id,
    )

    try:
        image_id = create_image_and_extern(conn, image_and_extern_init, store)
        created = True
    except (
        sqlite3.IntegrityError
    ):  # thrown if image with that extern raw file already exists
        image_id = lookup_image_id_from_hash_sha256(conn, raw_hash)
        created = False

    q = read_image_and_extern_hash(conn, image_id)
    return PostImageResponse(
        id=image_id,
        raw_info=ExternInfo(
            id=hexlify(q.raw.hash_sha256),
            extension=q.raw.extension,
            mimetype=q.raw.mimetype,
        ),
        web_info=ExternInfo(
            id=hexlify(q.web.hash_sha256),
            extension=q.web.extension,
            mimetype=q.web.mimetype,
        ),
        thumbnail_info=ExternInfo(
            id=hexlify(q.thumbnail.hash_sha256),
            extension=q.thumbnail.extension,
            mimetype=q.thumbnail.mimetype,
        ),
        uploaded_at=q.uploaded_at,
        created=created,
    )


@router.delete("/{image_id}")
async def handle_delete_image(
    image_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
) -> Response:
    conn = db.connect()
    delete_image(conn, image_id)
    return Response(status_code=200)
