import datetime
from binascii import hexlify
import pydantic
from fastapi import APIRouter, UploadFile, Response, status

from ..dependencies import DatabaseDep
from ...db.account import lookup_account_by_email
from ...db.session import create_session, SessionInit
from ...util.auth import gen_secret, check_password

router = APIRouter(prefix="/session")


class PostSessionRequest(pydantic.BaseModel):
    email: str
    password: str


@router.post("/")
def post_session(body: PostSessionRequest, db: DatabaseDep):
    conn = db.connect()
    account = lookup_account_by_email(conn, body.email)

    if account is None:
        return Response(status_code=401)

    if not check_password(
        password=body.password, passhash=account.passhash, salt=account.salt
    ):
        return Response(status_code=401)

    secret = gen_secret()
    expires_at = datetime.datetime.now() + datetime.timedelta(days=7)
    create_session(
        conn,
        SessionInit(account_id=account.rowid, secret=secret, expires_at=expires_at),
    )
    res = Response()

    res.set_cookie(
        "session-secret",
        value=hexlify(secret).decode(),
        expires=expires_at.isoformat(),
        secure=True,
        httponly=True,
        samesite="strict",
    )
    res.set_cookie(
        "session-email",
        value=account.email,
        expires=expires_at.isoformat(),
        secure=True,
        samesite="strict",
    )
    return res
