import datetime
from binascii import hexlify
from typing import Annotated

import pydantic
from fastapi import APIRouter, UploadFile, Response, status, Depends


from ..dependencies import DatabaseDep, AccountIdDep, ScopeRequirements
from ...db.photo import (
    PhotoInit,
    Photo,
    create_photo,
    read_photo,
    update_publish,
    PublishInfo,
    update_layout,
    update_time,
    update_location,
    update_description,
    find_photos,
    PhotoExtraImage,
    add_photo_extra_image,
    read_photo_extra_images,
)

from .extern import ExternInfo

router = APIRouter(prefix="/photo")


class PostPhotoBody(pydantic.BaseModel):
    front_image_id: int


class PostPhotoResponse(pydantic.BaseModel):
    id: int
    front_image_id: int
    created_at: datetime.datetime
    published: bool
    published_at: datetime.datetime | None
    location: str | None
    time: str | None
    layout: str | None
    description: str | None


@router.post("/")
def post_photo(
    body: PostPhotoBody,
    db: DatabaseDep,
    creator_id: AccountIdDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
) -> PostPhotoResponse:
    conn = db.connect()
    photo_id = create_photo(
        conn, PhotoInit(front_image_id=body.front_image_id, creator_id=creator_id)
    )
    q = read_photo(conn, photo_id)
    return PostPhotoResponse(
        id=q.rowid,
        front_image_id=q.front_image_id,
        created_at=q.created_at,
        published=q.published,
        published_at=q.published_at,
        location=q.location,
        time=q.time,
        layout=q.layout,
        description=q.description,
    )


class PhotoSearchEntry(pydantic.BaseModel):
    id: int
    thumbnail: ExternInfo


class SearchPhotoResponse(pydantic.BaseModel):
    photos: list[PhotoSearchEntry]


@router.get("/search")
def search_photos(
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> SearchPhotoResponse:
    conn = db.connect()
    q = find_photos(conn)
    return SearchPhotoResponse(
        photos=[
            PhotoSearchEntry(
                id=x.rowid,
                thumbnail=ExternInfo(
                    id=hexlify(x.thumbnail.hash_sha256),
                    extension=x.thumbnail.extension,
                    mimetype=x.thumbnail.mimetype,
                ),
            )
            for x in q
        ]
    )


class ExtraImageInfo(pydantic.BaseModel):
    image_id: int
    title: str
    added_at: datetime.datetime


class GetPhotoResponse(pydantic.BaseModel):
    id: int
    front_image_id: int
    extra_images: list[ExtraImageInfo]
    created_at: datetime.datetime
    published: bool
    published_at: datetime.datetime | None
    location: str | None
    time: str | None
    layout: str | None
    description: str | None


@router.get("/{photo_id}")
def get_photo(
    photo_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> GetPhotoResponse:
    conn = db.connect()
    q1 = read_photo(conn, photo_id)
    q2 = read_photo_extra_images(conn, photo_id)
    extra_images = [
        ExtraImageInfo(
            image_id=el.image_id,
            title=el.title,
            added_at=el.added_at,
        )
        for el in q2
    ]
    return GetPhotoResponse(
        id=q1.rowid,
        front_image_id=q1.front_image_id,
        extra_images=extra_images,
        created_at=q1.created_at,
        published=q1.published,
        published_at=q1.published_at,
        location=q1.location,
        time=q1.time,
        layout=q1.layout,
        description=q1.description,
    )


@router.post("/{photo_id}/publish")
def publish_photo(
    photo_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    photo = read_photo(conn, photo_id)
    if not photo.published:
        update_publish(
            conn,
            photo_id,
            PublishInfo(published=True, published_at=datetime.datetime.now()),
        )
    return Response(status_code=status.HTTP_200_OK)


@router.post("/{photo_id}/unpublish")
def unpublish_photo(
    photo_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    photo = read_photo(conn, photo_id)
    if photo.published:
        update_publish(conn, photo_id, PublishInfo(published=False, published_at=None))
    return Response(status_code=status.HTTP_200_OK)


@router.post("/{photo_id}/layout")
def post_photo_layout(
    photo_id: int,
    val: str,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    update_layout(conn, photo_id, val)
    return Response(status_code=status.HTTP_200_OK)


@router.post("/{photo_id}/time")
def post_photo_time(
    photo_id: int,
    val: str,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    update_time(conn, photo_id, val)
    return Response(status_code=status.HTTP_200_OK)


@router.post("/{photo_id}/location")
def post_photo_location(
    photo_id: int,
    val: str,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    update_location(conn, photo_id, val)
    return Response(status_code=status.HTTP_200_OK)


@router.post("/{photo_id}/description")
def post_photo_description(
    photo_id: int,
    val: str,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    update_description(conn, photo_id, val)
    return Response(status_code=status.HTTP_200_OK)


class PostPhotoExtraImageBody(pydantic.BaseModel):
    image_id: int
    title: str


@router.post("/{photo_id}/extra_image")
def post_photo_extra_image(
    photo_id: int,
    body: PostPhotoExtraImageBody,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
):
    conn = db.connect()
    add_photo_extra_image(
        conn,
        PhotoExtraImage(photo_id=photo_id, image_id=body.image_id, title=body.title),
    )
    return Response(status_code=status.HTTP_200_OK)


class GetPhotoExtraImagesResponse(pydantic.BaseModel):
    images: list[ExtraImageInfo]


@router.get("/{photo_id}/extra_images")
def get_photo_extra_images(
    photo_id: int,
    db: DatabaseDep,
    _: Annotated[None, Depends(ScopeRequirements(["modify_photos"]))],
) -> GetPhotoExtraImagesResponse:
    conn = db.connect()
    q = read_photo_extra_images(conn, photo_id)
    return GetPhotoExtraImagesResponse(
        images=[
            ExtraImageInfo(
                image_id=el.image_id,
                title=el.title,
                added_at=el.added_at,
            )
            for el in q
        ]
    )
