import base64
from binascii import unhexlify
from typing import Annotated

import pydantic
from fastapi import APIRouter, Depends

from ...db.extern import (
    lookup_extern_id_from_hash_sha256,
    read_extern,
)

from ..dependencies import DatabaseDep, ConfigDep, ExternStoreDep, ScopeRequirements


class ExternInfo(pydantic.BaseModel):
    id: str
    extension: str | None
    mimetype: str | None


class GetExternResponse(ExternInfo):
    data: str  # base64 encoded


router = APIRouter(prefix="/file")


@router.get("/{extern_id}")
def get_extern(
    extern_id: str,
    db: DatabaseDep,
    store: ExternStoreDep,
    _: Annotated[None, Depends(ScopeRequirements(["view_unpublished_content"]))],
) -> GetExternResponse:
    conn = db.connect()
    hash_sha256 = unhexlify(extern_id.encode())
    extern_row_id = lookup_extern_id_from_hash_sha256(conn, hash_sha256)
    q = read_extern(conn, extern_row_id, store)
    return GetExternResponse(
        data=base64.b64encode(q.data).decode(),
        id=extern_id,
        extension=q.extension,
        mimetype=q.mimetype,
    )
