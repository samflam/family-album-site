from fastapi import FastAPI, APIRouter, Response, status

from .routers.session import router as session_router
from .routers.extern import router as extern_router
from .routers.image import router as image_router
from .routers.photo import router as photo_router

app = FastAPI(root_path="/api")
app.include_router(session_router)
app.include_router(extern_router)
app.include_router(image_router)
app.include_router(photo_router)


@app.get("/")
def read_main() -> None:
    return Response(status_code=status.HTTP_200_OK)
