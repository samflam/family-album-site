import datetime
from dataclasses import dataclass
from functools import cache
import os
import tomllib
from pathlib import Path
from typing import Annotated
import sqlite3
import warnings
from collections.abc import Iterable
import binascii

import pydantic
from fastapi import Depends, status, Request
from fastapi.exceptions import HTTPException

from ..db.setup import connect
from ..db.session import lookup_session_from_secret
from ..db.scope import Scope
from ..db.role_scope import lookup_scopes_from_account_id
from ..util.mail import AbstractMailer, Mail, AwsMailer, StubMailer
from ..util.extern import AbstractExternStore, ExternStoreFs


CONFIG_ENV_NAME = "FAS_CONFIG_PATH"
DEFAULT_CONFIG_PATH = Path("./fas_backend.toml")


class Config(pydantic.BaseModel):
    base_fs_path: Path = Path(".")
    database_path: Path = Path("./db.sqlite")
    store_path: Path = Path("./extern")
    public_base_url: str = "http://localhost"
    aws_api_key: str | None = None
    # Dangerous! Disables checking if user has the required scope to
    # make an API request. May be useful for testing.
    check_scopes: bool = True


class Overrides(pydantic.BaseModel):
    model_config = pydantic.ConfigDict(arbitrary_types_allowed=True)
    store: AbstractExternStore | None = None
    mailer: AbstractMailer | None = None


overrides = Overrides()


def override_extern_store(store: AbstractExternStore):
    overrides.store = store


def override_mailer(mailer: AbstractMailer):
    overrides.mailer = mailer


# FIXME: caching is causes tests with different configs to interfere
# @cache
def get_config() -> Config:
    try:
        config_path = Path(os.environ[CONFIG_ENV_NAME])
    except KeyError:
        try:
            with open(DEFAULT_CONFIG_PATH, "rb") as f:
                file_config = tomllib.load(f)
        except FileNotFoundError:
            file_config = {}
    else:
        with open(config_path, "rb") as f:
            file_config = tomllib.load(f)

    return Config(**file_config)


ConfigDep = Annotated[Config, Depends(get_config)]


class Database(pydantic.BaseModel):
    path: Path

    def connect(self) -> sqlite3.Connection:
        conn = sqlite3.connect(self.path, autocommit=False)
        conn.execute("pragma foreign_keys = on")
        return conn


def get_database(config: ConfigDep) -> Database:
    return Database(path=config.base_fs_path / config.database_path)


DatabaseDep = Annotated[Database, Depends(get_database)]


def get_extern_store(config: ConfigDep) -> AbstractExternStore:
    if overrides.store:
        return overrides.store
    store_full_path = config.base_fs_path / config.store_path
    return ExternStoreFs(store_full_path)


ExternStoreDep = Annotated[AbstractExternStore, Depends(get_extern_store)]


def get_mailer(config: ConfigDep) -> AbstractMailer:
    if overrides.mailer:
        return overrides.mailer
    if not config.aws_api_key:
        warnings.warn("config aws_api_key is unset - no emails will be delivered")
        return StubMailer()
    return AwsMailer(config.aws_api_key)


MailerDep = Annotated[AbstractMailer, Depends(get_mailer)]


def maybe_get_account_id(request: Request, db: DatabaseDep) -> int | None:
    session_secret = request.cookies.get("session-secret", None)
    if not session_secret:
        return None
    try:
        session_secret = binascii.unhexlify(session_secret)
    except binascii.Error:
        return None
    conn = db.connect()
    session = lookup_session_from_secret(conn, session_secret)
    if not session:
        return None
    if session.expires_at < datetime.datetime.now():
        return None
    return session.account_id


MaybeAccountIdDep = Annotated[int, Depends(maybe_get_account_id)]


def get_account_id(maybe_account_id: MaybeAccountIdDep) -> int:
    if maybe_account_id is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )
    return maybe_account_id


AccountIdDep = Annotated[int, Depends(get_account_id)]


def get_scopes(account_id: MaybeAccountIdDep, db: DatabaseDep) -> list[Scope]:
    if account_id is None:
        return []
    conn = db.connect()
    return lookup_scopes_from_account_id(conn, account_id)


ScopesDep = Annotated[list[Scope], Depends(get_scopes)]


class ScopeRequirements:
    def __init__(self, scope_names: Iterable[str]):
        self.scope_names = frozenset(scope_names)

    def __call__(
        self, config: ConfigDep, maybe_account_id: MaybeAccountIdDep, scopes: ScopesDep
    ):
        if not config.check_scopes:
            return

        logged_in = maybe_account_id is not None
        scope_names = {scope.name for scope in scopes}

        if self.scope_names.issubset(scope_names):
            return

        if logged_in:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN, detail="Not allowed"
            )
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
        )
