from pathlib import Path
import sqlite3

import click
import pydantic

from ..db.setup import init_database, connect
from ..db.account import AccountInit, create_account, lookup_account_by_email
from ..db.role import lookup_role_by_name, read_all_roles

from ..util.auth import gen_salt, hash_password


def raise_error(msg):
    msg = str(msg)
    raise click.ClickException(click.style(msg, bold=True, fg="red"))


@click.group
def cli():
    pass


@cli.command
@click.argument("path", type=Path)
def initdb(path):
    if path.exists():
        raise_error(f"Path {path} already exists!")

    conn = sqlite3.connect(str(path), autocommit=False)
    init_database(conn)


@cli.command
@click.argument("db-path", type=Path)
@click.argument("role-name")
@click.option("--email")
@click.option("--password")
def add_account(db_path, role_name, email, password):
    if not db_path.exists():
        raise_error(f"No existing database at {db_path}!")

    conn = connect(db_path)

    role = lookup_role_by_name(conn, role_name)
    if role is None:
        all_roles = read_all_roles(conn)
        all_role_names = "".join([f"\n\t{role.name}" for role in all_roles])
        raise_error(f'Role "{role_name}" not found! Choices are: {all_role_names}')

    if email is None:
        email = click.prompt("Enter your email address")
    try:
        email = pydantic.validate_email(email)[1]
    except ValueError:
        raise_error(f'Email "{email}" is invalid!')

    if lookup_account_by_email(conn, email) is not None:
        raise raise_error(f"Email {email} already has an account!")

    if password is None:
        password = click.prompt(
            "Enter your password",
            hide_input=True,
            confirmation_prompt="Enter your password again",
        )

    salt = gen_salt()
    passhash = hash_password(password, salt)

    create_account(
        conn,
        AccountInit(
            email=email,
            salt=salt,
            passhash=passhash,
            role_id=role.rowid,
            wants_emails=True,
        ),
    )
