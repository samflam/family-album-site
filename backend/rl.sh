ACTIVATE_PATH="${RL_ROOT}/.venv/family_album_site/bin/activate"

if [ -f "${ACTIVATE_PATH}" ]
then
	source "${ACTIVATE_PATH}"
fi
