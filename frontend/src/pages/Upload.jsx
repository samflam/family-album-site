import React from "react";
import { useState } from "react";

import Navigation from "../components/Navigation.jsx";
import UploadDrag from "../components/UploadDrag.jsx";
import UploadBrowse from "../components/UploadBrowse.jsx";
import UploadList from "../components/UploadList.jsx";

export default ({ notify }) => {
  const [uploads, setUploads] = useState([]);
  return (
    <>
      <Navigation />
      {/*<UploadDrag uploads={uploads} setUploads={setUploads} />*/}
      <UploadBrowse uploads={uploads} setUploads={setUploads} />
      <UploadList uploads={uploads} notify={notify} />
    </>
  );
};
