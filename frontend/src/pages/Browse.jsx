import React from "react";

import { useState } from "react";

import Navigation from "../components/Navigation.jsx";
import SearchContainer from "../components/SearchContainer.jsx";

export default ({ notify }) => {
  return (
    <>
      <Navigation />
      <SearchContainer />
    </>
  );
};
