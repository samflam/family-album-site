import React from "react";
import HomeContainer from "../components/HomeContainer.jsx";
import LoginContainer from "../components/LoginContainer.jsx";

export default ({ loggedIn, setLoggedIn, notify }) => {
  let content;
  if (loggedIn) {
    content = <HomeContainer notify={notify} />;
  } else {
    content = <LoginContainer setLoggedIn={setLoggedIn} notify={notify} />;
  }

  return <div id="content=home">{content}</div>;
};
