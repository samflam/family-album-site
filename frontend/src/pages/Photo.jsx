import React from "react";
import { useParams } from "react-router-dom";

import Navigation from "../components/Navigation.jsx";
import PhotoInfoContainer from "../components/PhotoInfoContainer.jsx";

export default ({ notify }) => {
  const { id } = useParams();
  return (
    <>
      <Navigation />
      <PhotoInfoContainer id={id} />
    </>
  );
};
