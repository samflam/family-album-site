import React from "react";
import { useParams } from "react-router-dom";

import Navigation from "../components/Navigation.jsx";
import ImageInfoContainer from "../components/ImageInfoContainer.jsx";

export default ({ notify }) => {
  const { id } = useParams();
  return (
    <>
      <Navigation />
      <ImageInfoContainer id={id} />
    </>
  );
};
