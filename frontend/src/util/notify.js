let counter = 0;

export function genKey() {
  counter += 1;
  return `notify_${counter}`;
}

export function submitError({ notifications, setNotifications, message }) {
  const key = genKey();
  setNotifications([{ kind: "error", message, key }, ...notifications]);
}

export function submitInfo({ notifications, setNotifications, message }) {
  const key = genKey();
  setNotifications([{ kind: "info", message, key }, ...notifications]);
}

export function removeNotification({ notifications, setNotifications, key }) {
  setNotifications(notifications.filter((msg) => msg.key !== key));
}
