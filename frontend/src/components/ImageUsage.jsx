import React from "react";
import { useState, useEffect } from "react";

import CreatePhotoButton from "./CreatePhotoButton.jsx";
import PhotoUsageList from "./PhotoUsageList.jsx";

export default ({ imageId }) => {
  const [usage, setUsage] = useState(null);
  useEffect(async () => {
    const res = await fetch(`/api/image/${imageId}/usage`);
    // TODO: error handling
    const body = await res.json();
    setUsage(body.photos);
  }, [imageId]);
  return usage === null ? (
    <span>Loading usage...</span>
  ) : (
    (() => {
      const used = usage.length > 0;
      return (
        <>
          {used && <PhotoUsageList photos={usage} />}
          <CreatePhotoButton imageId={imageId} requireConfirm={used} />
        </>
      );
    })()
  );
};
