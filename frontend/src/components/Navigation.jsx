import React from "react";
import { Link } from "react-router-dom";

import Logo from "./Logo.jsx";

export default () => (
  <header>
    <Link id="logo" to="/">
      <Logo />
    </Link>
    <Link to="/browse">
      <span>Browse</span>
    </Link>
    <Link to="/upload">
      <span>Upload</span>
    </Link>
  </header>
);
