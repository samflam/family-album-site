import React from "react";

import NotificationDisplay from "./NotificationDisplay.jsx";
import { removeNotification } from "../util/notify.js";

export default ({ notifications, setNotifications }) => {
  const elems = notifications.map((notification) => {
    function remove() {
      removeNotification({
        notifications,
        setNotifications,
        key: notification.key,
      });
    }
    return <NotificationDisplay {...{ ...notification, remove }} />;
  });
  if (elems.length === 0) {
    return <></>;
  }

  function clear() {
    setNotifications([]);
  }
  return (
    <div className="notify-container">
      <button onClick={clear}>Dismiss all</button>
      {elems}
    </div>
  );
};
