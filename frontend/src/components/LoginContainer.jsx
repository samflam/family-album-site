import React from "react";

export default ({ setLoggedIn, notify }) => {
  const handleLogin = async (e) => {
    e.preventDefault();
    const email = e.target.elements.email.value;
    const password = e.target.elements.password.value;
    try {
      const res = await fetch("/api/session/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      });
      if (res.ok) {
        setLoggedIn(true);
      } else if (res.status === 401) {
        const message = "Login failed: incorrect username or password";
        notify.error(message);
        console.error(res);
      } else {
        const message = "Login failed: unknown error";
        notify.error(message);
        console.error(res);
      }
    } catch (err) {
      const message = "Login failed: unknown error";
      notify.error(message);
      console.error(err);
    }
  };
  return (
    <form onSubmit={handleLogin}>
      <input name="email" placeholder="email" />
      <input type="password" name="password" placeholder="password" />
      <button>Login</button>
    </form>
  );
};
