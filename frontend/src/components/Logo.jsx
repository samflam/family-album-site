import React from "react";

export default () => (
  <div>
    <img className="logo" src="/static/dev-logo.svg" title="fas-dev"></img>
  </div>
);
