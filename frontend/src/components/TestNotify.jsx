import React from "react";

import { submitError, submitInfo } from "../util/notify.js";

export default ({ notify }) => {
  function handleSubmitError() {
    notify.error("Test error");
  }

  function handleSubmitInfo() {
    notify.info("Test info");
  }

  return (
    <>
      <button onClick={handleSubmitError}>Notify error</button>
      <button onClick={handleSubmitInfo}>Notify info</button>
      <button onClick={notify.clear}>Notify clear</button>
    </>
  );
};
