import React from "react";

export default ({ id, thumbnailUrl, selectedId, setSelectedId }) => {
  const className = id === selectedId ? "selection selected" : "selection";

  function select() {
    setSelectedId(id);
  }

  return <img className={className} onClick={select} src={thumbnailUrl} />;
};
