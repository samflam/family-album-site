import React from "react";

export default ({ message, kind, remove }) => {
  const cssClass = (() => {
    switch (kind) {
      case "info":
        return "notify-info";
      case "error":
        return "notify-error";
      default:
        return "";
    }
  })();
  return (
    <div className={cssClass}>
      <span>{message}</span>
      <button onClick={remove}>X</button>
    </div>
  );
};
