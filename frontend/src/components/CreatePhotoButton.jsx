import React from "react";
import { useNavigate } from "react-router-dom";

export default ({ imageId, requireConfirm }) => {
  const navigate = useNavigate();
  async function createPhoto() {
    if (requireConfirm) {
      const confirmed = confirm(
        "Image already in use. Are you sure you want to create a photo?",
      );
      if (!confirmed) {
        return;
      }
    }
    const res = await fetch(`/api/photo/`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ front_image_id: imageId }),
    });
    // TODO: handle error
    const photo = await res.json();
    const photoUrl = `/photo/${photo.id}`;
    navigate(photoUrl);
  }

  return <button onClick={createPhoto}>Create photo page</button>;
};
