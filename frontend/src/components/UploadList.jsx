import React from "react";

import Upload from "./Upload.jsx";

export default ({ uploads, notify }) => {
  const uploadComponents = uploads.map((file, index) => (
    <Upload file={file} key={index} notify={notify} />
  ));
  return <div>{uploadComponents}</div>;
};
