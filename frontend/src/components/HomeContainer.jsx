import React from "react";

import Navigation from "./Navigation.jsx";
import TestNotify from "./TestNotify.jsx";
import TestModal from "./TestModal.jsx";

export default ({ notify }) => (
  <>
    <Navigation />
    <div>Welcome to the family album site development version</div>
    <TestModal />
    <TestNotify notify={notify} />
  </>
);
