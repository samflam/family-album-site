import React from "react";

import { useState, useEffect } from "react";

import ImageInfoDisplay from "./ImageInfoDisplay.jsx";

export default ({ id }) => {
  const [imageInfo, setImageInfo] = useState(null);
  useEffect(async () => {
    const res = await fetch(`/api/image/${id}`);
    // TODO: error handling
    const info = await res.json();
    setImageInfo(info);
  }, [id]);

  if (imageInfo === null) {
    return (
      <div>
        <span>Loading...</span>
      </div>
    );
  }

  function buildFileUrl({ id, extension }) {
    const filename = id + (extension ?? "");
    return `/f/${filename}`;
  }

  const { raw_info, web_info, uploaded_at } = imageInfo;

  const rawUrl = buildFileUrl(raw_info);
  const webUrl = buildFileUrl(web_info);

  return (
    <div>
      <ImageInfoDisplay
        rawUrl={rawUrl}
        webUrl={webUrl}
        uploadedAt={uploaded_at}
      />
    </div>
  );
};
