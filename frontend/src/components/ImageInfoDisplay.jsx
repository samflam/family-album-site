import React from "react";

export default ({ rawUrl, webUrl, uploadedAt }) => (
  <>
    <a href={webUrl}>
      <img className="image-display" src={webUrl}></img>
    </a>
    <p>
      <a href={rawUrl}>Raw image</a>
    </p>
    <p>Uploaded at {uploadedAt}</p>
  </>
);
