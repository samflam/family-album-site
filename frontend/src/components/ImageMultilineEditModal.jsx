import React from "react";

import { useState, useEffect } from "react";

import Modal from "./Modal.jsx";

export default ({ imageUrl, title, value, setValue }) => {
  const [editing, setEditing] = useState(false);
  const [inputValue, setInputValue] = useState(value);

  const modalTitle = `Edit ${title}`;

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  function handleChange(e) {
    setInputValue(e.target.value);
  }

  function handleClickEdit(e) {
    setEditing(true);
  }

  function onClickSave(e) {
    setValue(inputValue);
    setEditing(false);
  }

  function cancelWithConfirm(e) {
    if (value !== inputValue) {
      if (!confirm("Discard changes?")) {
        return;
      }
    }
    setInputValue(value);
    setEditing(false);
  }

  const maybeModal = editing && (
    <Modal title={modalTitle} cancel={cancelWithConfirm}>
      <div className="edit-modal">
        {imageUrl ? (
          <img className="edit-modal-image" src={imageUrl} />
        ) : (
          "Loading image"
        )}
        <div className="edit-modal-content">
          <textarea onChange={handleChange} rows="15" cols="50">
            {inputValue}
          </textarea>
          <br />
          <div className="edit-modal-buttons">
            <button onClick={onClickSave}>Save</button>
            <button onClick={cancelWithConfirm}>Cancel</button>
          </div>
        </div>
      </div>
    </Modal>
  );

  return (
    <>
      <div className="info-field">
        <span>
          <button onClick={handleClickEdit}>✏️</button>
          <b>{` ${title}:`}</b>
        </span>
        <p className="prewrap">{value}</p>
      </div>
      {maybeModal}
    </>
  );
};
