import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default () => {
  const [photos, setPhotos] = useState([]);

  useEffect(async () => {
    const res = await fetch("/api/photo/search");
    const { photos } = await res.json();
    setPhotos(
      photos.map((photo) => {
        const filename = photo.thumbnail.id + (photo.thumbnail.extension ?? "");
        return {
          key: photo.id,
          thumbnailUrl: `/f/${filename}`,
          pageUrl: `/photo/${photo.id}`,
        };
      }),
    );
  }, []);

  return (
    <div>
      {photos.map(({ key, pageUrl, thumbnailUrl }) => (
        <Link key={key} to={pageUrl}>
          <img src={thumbnailUrl}></img>
        </Link>
      ))}
    </div>
  );
};
