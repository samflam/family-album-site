import React from "react";
import { useState, useEffect } from "react";

import UploadDisplay from "./UploadDisplay.jsx";

export default ({ file, notify }) => {
  const [stage, setStage] = useState("pending");
  const [thumbnail, setThumbnail] = useState(undefined);

  useEffect(async () => {
    const formData = new FormData();

    formData.append("image_file", file);

    try {
      const res = await fetch("/api/image/", {
        method: "POST",
        body: formData,
      });

      if (!res.ok) {
        throw res;
      }

      const img = await res.json();

      const filename =
        img.thumbnail_info.id + (img.thumbnail_info.extension ?? "");
      setThumbnail(`/f/${filename}`);

      if (img.created) {
        setStage("success");
      } else {
        setStage("skipped");
      }
    } catch (err) {
      notify.error(`Error uploading ${file.name}`);
      console.error(err);
      setStage("error");
    }
  }, [file]);

  return <UploadDisplay name={file.name} stage={stage} thumbnail={thumbnail} />;
};
