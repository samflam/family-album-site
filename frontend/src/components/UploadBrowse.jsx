import React from "react";

export default ({ uploads, setUploads }) => {
  function onChange(e) {
    e.stopPropagation();
    e.preventDefault();
    const files = e.target.files;
    setUploads([...uploads, ...files]);
  }
  return <input type="file" multiple accept="image/*" onChange={onChange} />;
};
