import React from "react";

import { useState, useEffect } from "react";

import PhotoImageInfoContainer from "./PhotoImageInfoContainer.jsx";
import PhotoExtraImageAdd from "./PhotoExtraImageAdd.jsx";
import PhotoExtraImagesContainer from "./PhotoExtraImagesContainer.jsx";
import PhotoDetailsContainer from "./PhotoDetailsContainer.jsx";

export default ({ id }) => {
  const [photoInfo, setPhotoInfo] = useState(null);
  useEffect(async () => {
    const res = await fetch(`/api/photo/${id}`);
    // TODO: error handling
    const info = await res.json();
    setPhotoInfo(info);
  }, [id]);

  async function addExtraImage({ imageId, title }) {
    const postRes = await fetch(`/api/photo/${id}/extra_image`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ image_id: imageId, title }),
    });
    const getRes = await fetch(`/api/photo/${id}/extra_images`);
    const getResBody = await getRes.json();
    setPhotoInfo({ ...photoInfo, extra_images: getResBody.images });
  }

  async function setLayout(layout) {
    const res = await fetch(
      `/api/photo/${id}/layout?val=${encodeURIComponent(layout)}`,
      { method: "POST" },
    );
    // TODO: error handling
    setPhotoInfo({ ...photoInfo, layout });
  }

  async function setTime(time) {
    const res = await fetch(
      `/api/photo/${id}/time?val=${encodeURIComponent(time)}`,
      { method: "POST" },
    );
    // TODO: error handling
    setPhotoInfo({ ...photoInfo, time });
  }

  async function setLocation(location) {
    const res = await fetch(
      `/api/photo/${id}/location?val=${encodeURIComponent(location)}`,
      { method: "POST" },
    );
    // TODO: error handling
    setPhotoInfo({ ...photoInfo, location });
  }

  async function setDescription(description) {
    const res = await fetch(
      `/api/photo/${id}/description?val=${encodeURIComponent(description)}`,
      { method: "POST" },
    );
    // TODO: error handling
    setPhotoInfo({ ...photoInfo, description });
  }

  if (photoInfo === null) {
    return (
      <div>
        <span>Loading...</span>
      </div>
    );
  }

  const extraImages = photoInfo.extra_images
    .toSorted((a, b) => a.added_at < b.added_at)
    .map(({ image_id, title }) => {
      return { id: image_id, title };
    });

  return (
    <div>
      <PhotoImageInfoContainer id={photoInfo.front_image_id} />
      <PhotoDetailsContainer
        photoId={id}
        layout={photoInfo.layout}
        setLayout={setLayout}
        time={photoInfo.time}
        setTime={setTime}
        location={photoInfo.location}
        setLocation={setLocation}
        description={photoInfo.description}
        setDescription={setDescription}
      />
      <PhotoExtraImageAdd photoId={id} addExtraImage={addExtraImage} />
      <PhotoExtraImagesContainer extraImages={extraImages} />
    </div>
  );
};
