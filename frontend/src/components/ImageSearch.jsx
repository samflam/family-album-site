import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default () => {
  const [images, setImages] = useState([]);

  useEffect(async () => {
    const res = await fetch("/api/image/search");
    const { images } = await res.json();
    setImages(
      images.map((img) => {
        const filename =
          img.thumbnail_info.id + (img.thumbnail_info.extension ?? "");
        return {
          key: img.id,
          thumbnailUrl: `/f/${filename}`,
          pageUrl: `/image/${img.id}`,
        };
      }),
    );
  }, []);

  return (
    <div>
      {images.map(({ key, pageUrl, thumbnailUrl }) => (
        <Link key={key} to={pageUrl}>
          <img src={thumbnailUrl}></img>
        </Link>
      ))}
    </div>
  );
};
