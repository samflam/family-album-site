import React from "react";

import { useEffect } from "react";

export default ({ children, title, cancel }) => {
  // Cancel if user presses escape
  useEffect(() => {
    document.addEventListener("keydown", handleKeydown);
    return () => {
      document.removeEventListener("keydown", handleKeydown);
    };
  }, [cancel]);

  useEffect(() => {
    document.body.classList.add("no-scroll");
    return () => {
      document.body.classList.remove("no-scroll");
    };
  });

  function handleKeydown(event) {
    if (event.key === "Escape") {
      cancel();
    }
  }

  // Cancel if user clicks outside the modal window
  function handleClickCancel(event) {
    // Check if the click was strictly on the blocker
    if (event.currentTarget === event.target) {
      cancel();
    }
  }

  return (
    <div className="modal-blocker" onClick={handleClickCancel}>
      <div className="modal-window">
        <div className="modal-header">
          <span className="modal-title">{title}</span>
          <button className="cancel-button" onClick={cancel}>
            X
          </button>
        </div>
        <hr />
        <div className="modal-content">{children}</div>
      </div>
    </div>
  );
};
