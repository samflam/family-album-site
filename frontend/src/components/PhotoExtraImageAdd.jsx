import React from "react";

import { useState } from "react";

import Modal from "./Modal.jsx";
import ExtraImageForm from "./ExtraImageForm.jsx";

export default ({ photoId, addExtraImage }) => {
  const [active, setActive] = useState(false);

  function activate() {
    setActive(true);
  }

  function deactivate() {
    setActive(false);
  }

  function submit({ imageId, title }) {
    addExtraImage({ imageId, title });
    deactivate();
  }

  return (
    <>
      <button onClick={activate}>Add supplemental image</button>
      {active ? (
        <Modal title="New supplemental image" cancel={deactivate}>
          <ExtraImageForm submit={submit} cancel={deactivate} />
        </Modal>
      ) : null}
    </>
  );
};
