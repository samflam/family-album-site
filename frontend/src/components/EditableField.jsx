import React from "react";
import { useState, useEffect } from "react";

export default ({ title, value, setValue }) => {
  value = value ?? "";
  const [mode, setMode] = useState("display");
  const [inputValue, setInputValue] = useState(value);

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  if (mode === "display") {
    function edit() {
      setMode("edit");
    }
    return (
      <span>
        {`${title}: ${value}`}
        <button onClick={edit}>✏️</button>
      </span>
    );
  }
  if (mode === "edit") {
    function onChange(e) {
      setInputValue(e.target.value);
    }
    function cancel() {
      setInputValue(value);
      setMode("display");
    }
    function submit() {
      setValue(inputValue);
      setMode("display");
    }
    return (
      <span>
        {`${title}: `}
        <input value={inputValue} onChange={onChange} />
        <button onClick={submit}>✅</button>
        <button onClick={cancel}>❌</button>
      </span>
    );
  }
};
