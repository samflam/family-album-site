import React from "react";

import { useState, useEffect } from "react";

import EditableField from "./EditableField.jsx";
import ImageLineEditModal from "./ImageLineEditModal.jsx";
import ImageMultilineEditModal from "./ImageMultilineEditModal.jsx";

export default ({
  photoId,
  layout,
  setLayout,
  time,
  setTime,
  location,
  setLocation,
  description,
  setDescription,
}) => {
  const [imageUrl, setImageUrl] = useState(null);

  useEffect(async () => {
    const photoInfoRes = await fetch(`/api/photo/${photoId}`);
    const photoInfo = await photoInfoRes.json();
    const imageId = photoInfo.front_image_id;
    const imageInfoRes = await fetch(`/api/image/${imageId}`);
    const imageInfo = await imageInfoRes.json();
    const { id, extension } = imageInfo.web_info;
    const filename = id + (extension ?? "");
    setImageUrl(`/f/${filename}`);
  }, [photoId]);

  return (
    <div>
      <div>
        <ImageLineEditModal
          imageUrl={imageUrl}
          title="Layout"
          value={layout}
          setValue={setLayout}
        />
      </div>
      <div>
        <ImageLineEditModal
          imageUrl={imageUrl}
          title="Time"
          value={time}
          setValue={setTime}
        />
      </div>
      <div>
        <ImageLineEditModal
          imageUrl={imageUrl}
          title="Location"
          value={location}
          setValue={setLocation}
        />
      </div>
      <div>
        <ImageMultilineEditModal
          imageUrl={imageUrl}
          title="Description"
          value={description}
          setValue={setDescription}
        />
      </div>
    </div>
  );
};
