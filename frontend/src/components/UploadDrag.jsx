import React from "react";

export default ({ uploads, setUploads }) => {
  function onDragOver(e) {
    // Needed for the onDrop handler to fire
    e.stopPropagation();
    e.preventDefault();
  }
  function onDrop(e) {
    e.stopPropagation();
    e.preventDefault();
    const files = e.dataTransfer.files;
    setUploads([...uploads, ...files]);
  }
  return (
    <div
      style={{ width: "100px", height: "100px", border: "1px solid black" }}
      onDragOver={onDragOver}
      onDrop={onDrop}
    >
      Drag here!
    </div>
  );
};
