import React from "react";

export default ({ name, stage, thumbnail }) => {
  const cssClass = (() => {
    switch (stage) {
      case "pending":
        return "upload-pending";
      case "success":
        return "upload-success";
      case "skipped":
        return "upload-skipped";
      case "error":
        return "upload-error";
      default:
        return "";
    }
  })();

  return (
    <div className={cssClass}>
      {thumbnail && <img src={thumbnail}></img>}
      <span>{`Image ${name} ${stage}`}</span>
    </div>
  );
};
