import React from "react";

import PhotoImageInfoContainer from "./PhotoImageInfoContainer.jsx";

export default ({ extraImages }) => {
  const elems = extraImages.map(({ id, title }) => {
    return (
      <div key={id}>
        <p>{title}</p>
        <PhotoImageInfoContainer id={id} />
      </div>
    );
  });

  if (extraImages.length === 0) {
    return <></>;
  }

  return (
    <div>
      <p>Supplemental images:</p>
      {elems}
    </div>
  );
};
