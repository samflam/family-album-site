import React from "react";

import { useState, useEffect } from "react";

import ImagePicker from "./ImagePicker.jsx";

export default ({ notify, submit, cancel }) => {
  const [selectedId, setSelectedId] = useState(null);
  const [title, setTitle] = useState(null);
  const [images, setImages] = useState(null);

  useEffect(async () => {
    const res = await fetch("/api/image/search");
    const { images } = await res.json();
    setImages(
      images.map((img) => {
        const filename =
          img.thumbnail_info.id + (img.thumbnail_info.extension ?? "");
        return {
          id: img.id,
          thumbnailUrl: `/f/${filename}`,
        };
      }),
    );
  }, []);

  const picker =
    images === null ? (
      <span>Loading images...</span>
    ) : (
      <ImagePicker
        images={images}
        selectedId={selectedId}
        setSelectedId={setSelectedId}
      />
    );

  function handleTitleChange(event) {
    setTitle(event.target.value);
  }

  function submitForm() {
    submit({ imageId: selectedId, title });
  }

  const canSubmit = !!selectedId && !!title;

  return (
    <>
      <button disabled={canSubmit ? undefined : true} onClick={submitForm}>
        Submit
      </button>
      <button onClick={cancel}>Cancel</button>
      <br />
      <label htmlFor="title">Title:</label>
      <input
        size="30"
        maxLength="40"
        name="title"
        type="text"
        onChange={handleTitleChange}
      />
      {title ? <></> : <span className="attention"> *title required</span>}
      <br />
      {selectedId ? (
        <span>Image #{selectedId} selected</span>
      ) : (
        <span className="attention">Select an image</span>
      )}
      {picker}
    </>
  );
};
