import React from "react";

import ImagePickerItem from "./ImagePickerItem.jsx";

export default ({ images, selectedId, setSelectedId }) => {
  const items = images.map(({ id, thumbnailUrl }) => (
    <ImagePickerItem
      key={id}
      id={id}
      thumbnailUrl={thumbnailUrl}
      selectedId={selectedId}
      setSelectedId={setSelectedId}
    />
  ));

  return <div>{items}</div>;
};
