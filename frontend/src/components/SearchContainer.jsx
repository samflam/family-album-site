import React from "react";
import { useLocation, useNavigate } from "react-router-dom";

import queryString from "query-string";

import PhotoSearch from "../components/PhotoSearch.jsx";
import ImageSearch from "../components/ImageSearch.jsx";

export default () => {
  const location = useLocation();
  const navigate = useNavigate();

  const queryParams = queryString.parse(location.search);
  const category = queryParams.cat ?? "photo";

  const categorySearch = (() => {
    switch (category) {
      case "photo":
        return <PhotoSearch />;
      case "image":
        return <ImageSearch />;
      default:
        return <span>Invalid category</span>;
    }
  })();

  function setCategoryPhoto() {
    navigate("/browse?cat=photo");
  }

  function setCategoryImage() {
    navigate("/browse?cat=image");
  }

  return (
    <>
      <span>
        Category:
        <button disabled={category === "photo"} onClick={setCategoryPhoto}>
          Photo
        </button>
        <button disabled={category === "image"} onClick={setCategoryImage}>
          Image
        </button>
      </span>
      {categorySearch}
    </>
  );
};
