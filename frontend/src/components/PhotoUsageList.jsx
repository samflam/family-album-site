import React from "react";

import { Link } from "react-router-dom";

export default ({ photos }) => {
  if (photos.length === 0) {
    return <div>{"Not used in any photos"}</div>;
  }
  if (photos.length === 1) {
    return (
      <div>
        {"Used in photo: "}
        <Link to={`/photo/${photos[0]}`}>{photos[0]}</Link>
      </div>
    );
  }
  return (
    <div>
      {`Used in ${photos.length} photos:`}
      <ul>
        {photos.map((photoId) => (
          <li key={photoId}>
            <Link to={`/photo/${photoId}`}>{photoId}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};
