import React from "react";

import { useState } from "react";

import Modal from "./Modal.jsx";

export default () => {
  const [showModal, setShowModal] = useState(false);
  function enableModal() {
    setShowModal(true);
  }
  function disableModal() {
    setShowModal(false);
  }

  return (
    <div>
      <button onClick={enableModal}>Show modal</button>
      {showModal && (
        <Modal cancel={disableModal} title="test title">
          Test modal
        </Modal>
      )}
    </div>
  );
};
