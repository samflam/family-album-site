import React from "react";
import style from "./style/application.scss";
import { useState } from "react";

import Home from "./pages/Home.jsx";
import Browse from "./pages/Browse.jsx";
import Upload from "./pages/Upload.jsx";
import Photo from "./pages/Photo.jsx";
import Image from "./pages/Image.jsx";
import NotificationContainer from "./components/NotificationContainer.jsx";

import { submitInfo, submitError } from "./util/notify.js";

import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";

export default () => {
  const [loggedIn, setLoggedIn] = useState(
    (() => {
      const cookies = document.cookie
        .split(";")
        .filter((el) => !!el)
        .map((token) => token.split("="))
        .reduce((acc, token) => {
          acc[decodeURIComponent(token[0].trim())] = decodeURIComponent(
            token[1].trim(),
          );
          return acc;
        }, {});
      const user = cookies["session-email"] ?? null;
      return user !== null;
    })(),
  );

  const [notifications, setNotifications] = useState([]);

  const notify = {
    error: (message) => {
      submitError({ notifications, setNotifications, message });
    },
    info: (message) => {
      submitInfo({ notifications, setNotifications, message });
    },
    clear: () => {
      setNotifications([]);
    },
  };

  return (
    <>
      <NotificationContainer {...{ notifications, setNotifications }} />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={<Home {...{ loggedIn, setLoggedIn, notify }} />}
          />
          <Route
            path="/browse"
            element={
              loggedIn ? (
                <Browse notify={notify} />
              ) : (
                <Navigate replace to="/" />
              )
            }
          />
          <Route
            path="/upload"
            element={
              loggedIn ? (
                <Upload notify={notify} />
              ) : (
                <Navigate replace to="/" />
              )
            }
          />
          <Route
            path="/photo/:id"
            element={
              loggedIn ? <Photo notify={notify} /> : <Navigate replace to="/" />
            }
          />
          <Route
            path="/image/:id"
            element={
              loggedIn ? <Image notify={notify} /> : <Navigate replace to="/" />
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
};
