const path = require("path");
const express = require("express");
const app = express();

const PORT = 8002;

const API_SERVER = "http://localhost:8001";

app.use("/static", express.static(path.resolve(__dirname, "static")));
app.use("/", express.static(path.resolve(__dirname, "dist")));

app.get("/upload", (req, res) =>
  res.sendFile(path.resolve(__dirname, "dist/index.html")),
);
app.get("/photo/:id", (req, res) =>
  res.sendFile(path.resolve(__dirname, "dist/index.html")),
);
app.get("/image/:id", (req, res) =>
  res.sendFile(path.resolve(__dirname, "dist/index.html")),
);
app.get("/browse", (req, res) =>
  res.sendFile(path.resolve(__dirname, "dist/index.html")),
);

app.get("/f/:filename", async (req, res) => {
  const { filename } = req.params;
  const file_id = filename.split(".")[0];

  // TODO: error handling
  const api_res = await fetch(`${API_SERVER}/api/file/${file_id}`, {
    headers: {
      cookie: req.get("cookie"),
    },
  });

  const { extension, mimetype, data } = await api_res.json();
  const src_filename = file_id + (extension ?? "");

  if (filename !== src_filename) {
    res.status(404).send("Not found");
    return;
  }

  const buffer = Buffer.from(data, "base64");
  if (mimetype) {
    res.setHeader("Content-Type", mimetype);
  }

  res.send(buffer);
});

app.listen(PORT, () => {
  console.log(`Listening on ${PORT}`);
});
